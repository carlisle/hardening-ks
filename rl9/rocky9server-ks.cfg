##############################################################################
## rocky9server
## Copyright (C) 2024 Carlisle Childress 
## carlisle.bitbucket@gmail.com
## 
##############################################################################
## This file tested with RockyLinux 9.5
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## Guide to the Secure Configuration of Red Hat Enterprise Linux 9
## https://static.open-scap.org/ssg-guides/ssg-rhel9-guide-index.html
##
## CIS Rocky Linux 9 benchmarks
## https://www.cisecurity.org/cis-benchmarks/
##
## These scripts are inspired by various Benchmarks by The Center for Internet
## Security. However, these scripts have not been reviewed or approved by CIS
## and they do not guarantee that their use will result in compliance with the
## CIS baseline. 
##
## This benchmark is a direct port of a SCAP Security Guide benchmark developed
## for Red Hat Enterprise Linux. It has been modified through an automated
## process to remove specific dependencies on Red Hat Enterprise Linux and to
## function with Rocky Linux. The result is a generally useful SCAP Security
## Guide benchmark with the following caveats:
##
## Rocky Linux is not an exact copy of Red Hat Enterprise Linux. There may be
## configuration differences that produce false positives and/or false
## negatives. If this occurs please file a bug report.
##
## Rocky Linux is a Linux distribution produced by the Rocky Enterprise Software
## Foundation. It is a free and open source operating system based on Red Hat
## Enterprise Linux with the goal "to be 100% bug-for-bug compatible" with its
## upstream commercial distribution. Rocky Linux has its own build system,
## compiler options, patchsets, and is a community supported, non-commercial
## operating system. Rocky Linux does not inherit certifications or evaluations
## from Red Hat Enterprise Linux. As such, some configuration rules (such as
## those requiring FIPS 140-2 encryption) will continue to fail on Rocky Linux.
##
## Members of the Rocky Linux community are invited to participate in OpenSCAP
## and SCAP Security Guide development. Bug reports and patches can be sent to
## GitHub: https://github.com/ComplianceAsCode/content. The mailing list is at
## https://fedorahosted.org/mailman/listinfo/scap-security-guide.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this guidance
## assume no responsibility whatsoever for its use by other parties, and makes
## no guarantees, expressed or implied, about its quality, reliability, or any
## other characteristic.
##
##############################################################################
## Notes
##
## Usage: Create a virtual machine of at least 2 cpus, 3 GB Ram and
## a virtual disk of at least 20 GB in size using VirtIO driver
##
## vmlinuz initrd=initrd.img inst.ks=https://bitbucket.org/carlisle/hardening-ks/raw/master/rl9/rocky9server-ks.cfg
##
##############################################################################
#version=RockyLinux9

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/rl9/rocky9-repos.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/epel9-repos.cfg

# Use graphical install
graphical

# Keyboard layouts
keyboard --xlayouts='us'

# System language
lang en_US.UTF-8

# Run the Setup Agent on first boot
firstboot --enable

# Network information
network  --bootproto=dhcp --device=ens3 --ipv6=auto --activate
network  --hostname=rocky9server

# Disk partitioning information
ignoredisk --only-use=vda
# Partition clearing information
clearpart --all --initlabel --drives=vda
zerombr

# On production systems boot should be increased to 1024
part /boot --fstype="xfs" --asprimary --size=512 --fsoptions="nodev,nosuid"
part pv.253002 --fstype="lvmpv" --grow --size=15616
volgroup vg0 --pesize=4096 pv.253002

logvol / --fstype="xfs" --name=root --vgname=vg0 --size=8192
logvol swap --name=swap --vgname=vg0 --size=256

# Ensure /home Located On Separate Partition
logvol /home  --fstype="xfs" --size=2048 --name=home --vgname=vg0 --fsoptions="nodev,nosuid"

# Ensure /var Located On Separate Partition
# Certain applications, include any flatpak get installed under /var 
# and setting noexec will prevent them from working
logvol /var   --fstype="xfs"  --size=3072  --name=var  --vgname=vg0 --fsoptions="nodev,nosuid,noexec"

# Ensure /var/log Located On Separate Partition
logvol /var/log  --fstype="xfs" --size=1024 --name=log --vgname=vg0 --fsoptions="nodev,nosuid,noexec"

# Ensure /var/log/audit Located On Separate Partition
logvol /var/log/audit  --fstype="xfs" --size=512 --name=audit --vgname=vg0 --fsoptions="nodev,nosuid,noexec"

# Ensure /tmp Located On Separate Partition
# Note: Install scripts for some software may assume /tmp is executable
# The filesystem can be changed without umounting using the command: mount -o exec,remount /tmp
logvol /tmp   --fstype="xfs" --size=512   --name=tmp  --vgname=vg0 --fsoptions="nodev,nosuid,noexec"

# Ensure /var/tmp Located On Separate Partition
logvol /var/tmp   --fstype="xfs" --size=512   --name=vartmp  --vgname=vg0 --fsoptions="nodev,nosuid,noexec"


# System timezone
timezone America/New_York --utc
timesource --ntp-pool=pool.ntp.org
timesource --ntp-server=time.cloudflare.com --nts


#Root password
###rootpw --lock
rootpw --plaintext abc123
user --name=boss --password=abc123 --plaintext --gecos="boss" --groups=wheel
user --name=user --password=abc123 --plaintext --gecos="user"

reboot


%packages
#@^minimal-environment
@^server-product-environment

%end

%addon com_redhat_kdump --disable
%end


#################
## POST INSTALL
#################

%post --log=/root/rocky9server.log

cat /proc/cmdline > /root/install.ks-boot_parameters

if grep -i -q "para=[a-zA-Z0-9]" /proc/cmdline
then
    THIS_PARA=`cat /proc/cmdline | sed 's/.*para=\([^ ]*\).*/\1/'`
fi

#timestamp
echo "** rocky9server START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi
/bin/cp -LRipd /etc ${BACKUPDIR}/etc-$(date +%F)-DEFAULT

#timestamp
echo "** rocky9server COMPLETE" $(date +%F-%H%M-%S)

%end

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/rl9/rl9-oscap.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-banner.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-ssh.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-shell.cfg

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-admin_notes.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-dnf-auto.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-tmux.cfg

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-aide.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-sudo.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-chrony.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-mail.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-syslog.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-firewalld.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-cron.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-crypto-policies.cfg

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-fstab.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-sysctl.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-modprobe.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-coredump.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-systemd.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-audit.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-grub.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-pam.cfg
##%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-pam_sssd.cfg

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-usb-storage.cfg
#%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-clamav.cfg

# For servers
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-services.cfg

# For workstations and laptops
#%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-gnome.cfg
#%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-applications.cfg
#%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-flatpak.cfg
#%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-usbguard.cfg
#%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-fapolicy.cfg

