##############################################################################
## rocky9ws-ks.cfg
## Creates a minimal Rocky Linux 9 Workstation installation 
## Copyright (C) 2023 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this 
## guidance assume no responsibility whatsoever for its use by other parties,
## and makes no guarantees, expressed or implied, about its quality, 
## reliability, or any other characteristic.
##
##############################################################################
## Notes
##
## https://docs.rockylinux.org/
## https://docs.rockylinux.org/release_notes/9_1/
## https://docs.rockylinux.org/guides/installation/
## https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9
##
## Usage: Create a virtual machine of at least 2 cpus, 3 GB Ram and
## a virtual disk of at least 20 GB in size using VirtIO driver
##
## vmlinuz initrd=initrd.img inst.ks=https://bitbucket.org/carlisle/hardening-ks/raw/master/rl9/rocky9ws-ks.cfg
##
##############################################################################
#version=RockyLinux9

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/rl9/rocky9-repos.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/epel9-repos.cfg

# Use graphical install
graphical

# Keyboard layouts
keyboard --xlayouts='us'

# System language
lang en_US.UTF-8

# Run the Setup Agent on first boot
firstboot --enable

# Generated using Blivet version 3.4.0
ignoredisk --only-use=vda

# Network information
network  --bootproto=dhcp --device=ens3 --ipv6=auto --activate
network  --hostname=rocky9ws

# Disk partitioning information
ignoredisk --only-use=vda
# Partition clearing information
clearpart --all --initlabel --drives=vda
zerombr

# On production systems boot should be increased to 1024
part /boot --fstype="xfs" --asprimary --size=512 --fsoptions="nodev,nosuid"
part pv.253002 --fstype="lvmpv" --grow --size=15616
volgroup vg0 --pesize=4096 pv.253002

logvol / --fstype="xfs" --name=root --vgname=vg0 --size=8192
logvol swap --name=swap --vgname=vg0 --size=256

# Ensure /home Located On Separate Partition
logvol /home  --fstype="xfs" --size=2048 --name=home --vgname=vg0 --fsoptions="nodev,nosuid"

# Ensure /var Located On Separate Partition
# Certain applications, include any flatpak get installed under /var 
# and setting noexec will prevent them from working
logvol /var   --fstype="xfs"  --size=3072  --name=var  --vgname=vg0 --fsoptions="nodev,nosuid,noexec"

# Ensure /var/log Located On Separate Partition
logvol /var/log  --fstype="xfs" --size=1024 --name=log --vgname=vg0 --fsoptions="nodev,nosuid,noexec"

# Ensure /var/log/audit Located On Separate Partition
logvol /var/log/audit  --fstype="xfs" --size=512 --name=audit --vgname=vg0 --fsoptions="nodev,nosuid,noexec"

# Ensure /tmp Located On Separate Partition
# Note: Install scripts for some software may assume /tmp is executable
# The filesystem can be changed without umounting using the command: mount -o exec,remount /tmp
logvol /tmp   --fstype="xfs" --size=512   --name=tmp  --vgname=vg0 --fsoptions="nodev,nosuid,noexec"

# Ensure /var/tmp Located On Separate Partition
logvol /var/tmp   --fstype="xfs" --size=512   --name=vartmp  --vgname=vg0 --fsoptions="nodev,nosuid,noexec"


# System timezone
timezone America/New_York --utc
timesource --ntp-server=0.us.pool.ntp.org


#Root password
rootpw --lock
user --name=boss --password=abc123 --plaintext --gecos="boss" --groups=wheel
user --name=user --password=abc123 --plaintext --gecos="user"


reboot

%packages
@^workstation-product-environment

%end


%addon com_redhat_kdump --enable --reserve-mb='auto'

%end


#################
## POST INSTALL
#################

%post --log=/root/rocky9ws-ks.cfg.log

cat /proc/cmdline > /root/install.ks-boot_parameters

if grep -i -q "para=[a-zA-Z0-9]" /proc/cmdline
then
    THIS_PARA=`cat /proc/cmdline | sed 's/.*para=\([^ ]*\).*/\1/'`
fi

#timestamp
echo "** rocky9ws-ks.cfg START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

/bin/cp -LRipd /etc ${BACKUPDIR}/etc-$(date +%F)-DEFAULT



#timestamp
echo "** rocky9ws-ks.cfg COMPLETE" $(date +%F-%H%M-%S)

%end

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/rl9/rl9-oscap.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-shell-077.cfg

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-admin_notes.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-dnf-auto.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-tmux.cfg

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-banner.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-ssh.cfg

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-gnome.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-applications.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-flatpak.cfg

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-aide.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-sudo.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-chrony.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-mail.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-syslog.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-firewalld.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-cron.cfg

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-fstab.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-sysctl.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-modprobe.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-coredump.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-systemd.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-audit.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-grub.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-pam.cfg

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-usb-storage.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-usbguard.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-fapolicy.cfg
%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el8/el8-clamav.cfg

%include https://bitbucket.org/carlisle/hardening-ks/raw/master/el9/el9-ossec.cfg

