#!/usr/bin/env bash

# This evaluates against SCAP 0.1.74 provided by the scap-security-guide-0.1.74-1 rpm
# Release Notes: https://github.com/OpenSCAP/scap-security-guide/releases/tag/v0.1.74

# Evaluate all selected profiles

# Ensure packages are up to date
# dnf update openscap scap-security-guide

HOST=$(hostname)
TIMESTAMP=$(date +%F-%H%M)
SCAPVER=0.1.74
LABEL=${HOST}-${TIMESTAMP}-${SCAPVER}

TARGETDIR=/root/oscap_results
OS=almalinux10

# Manual usage:
# Set the above varibles
# if [ ! -d "${TARGETDIR}" ]; then mkdir -p ${TARGETDIR}; fi
# cd ${TARGETDIR}
#
# curl -O https://bitbucket.org/carlisle/hardening-ks/raw/master/${OS}/oscap-evaluate_${OS}_${SCAPVER}.sh
# chmod 700 oscap-evaluate_${OS}_${SCAPVER}.sh
# dnf install scap-security-guide tar bzip2
# ionice -c2 -n7 nice -n 18 ./oscap-evaluate_${OS}_${SCAPVER}.sh &> scap-${LABEL}.log &

# Use content from installed package
CONTENT=/usr/share/xml/scap/ssg/content

# To extract the list of profiles:
#oscap info ${CONTENT}/ssg-${OS}-ds.xml | grep profile | sed 's+.*profile_++'

PARRAY=(
# Center for Internet Security Benchmarks
     cis
#     cis_server_l1
#     cis_workstation_l1
#     cis_workstation_l2
# Payment Card Industry Data Security Standard
#    pci-dss
## US DoD Controlled Unclassified Information
##    cui
# Health Insurance Portability and Accountability Act
#    hipaa
# Australian Cyber Security Centre Essential Eight
#    e8
# Australian Cyber Security Centre (ACSC) ISM Official
#    ism_o
#    ism_o_secret
#    ism_o_top_secret
# United States Government Configuration Baseline (USGCB / STIG)
#    ospp
# Security Technical Implementation Guide (STIG)
    stig
#    stig_gui
# Agence nationale de la sécurité des systèmes d'information
#    anssi_bp28_minimal
#    anssi_bp28_intermediary
#    anssi_bp28_enhanced
#    anssi_bp28_high
## Centro Criptológico Nacional STIC
##    ccn_basic
##    ccn_intermediate
##    ccn_advanced

     )

# Download oval file if it doesn't exist

# OVALURL="https://security.almalinux.org/oval/"
#OVALFILE="org.almalinux.alsa-10.xml.bz2"

#if [ ! -r ${TARGETDIR}/${OVALFILE} ]; then
#   echo "File ${TARGETDIR}/${OVALFILE} does not exist. Attempting to download it"
#   if curl --fail -o ${TARGETDIR}/${OVALFILE} ${OVALURL}/${OVALFILE}; then
#       bunzip2 ${TARGETDIR}/${OVALFILE}
#   else
#       echo "Download failed"
#       exit 1
#   fi
#else
#   echo "File ${TARGETDIR}/${OVALFILE} exists."
#fi


for PROFILE in "${PARRAY[@]}"; do

        printf "\n#### %s ####\n\n" ${PROFILE}

# using remote resources
#    oscap xccdf eval --fetch-remote-resources  --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
#     --results ${TARGETDIR}/${LABEL}-${PROFILE}.xml \
#     --report  ${TARGETDIR}/${LABEL}-${PROFILE}.html \
#     ${CONTENT}/ssg-${OS}-ds.xml;

    # Evaluate each profile using previously downloaded oval file
           oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
         --results ${TARGETDIR}/${LABEL}-${PROFILE}.xml \
         --report  ${TARGETDIR}/${LABEL}-${PROFILE}.html \
         ${CONTENT}/ssg-${OS}-ds.xml;

    # Generate remediation script for each profile

    oscap xccdf generate fix \
     --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
     --output ${TARGETDIR}/remediation-${LABEL}-${PROFILE}.sh \
     ${CONTENT}/ssg-${OS}-ds.xml;

    # Generate Guide for each profile

    oscap xccdf generate guide --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
      --output ${TARGETDIR}/guide-${LABEL}-${PROFILE}.html \
      ${CONTENT}/ssg-${OS}-ds.xml;

done

tar czf ${LABEL}.tar.gz oscap-evaluate*.sh ${LABEL}-*.xml ${LABEL}-*.html remediation-${LABEL}-*.sh guide-${LABEL}-*.html

printf "\nDONE"

# END 
