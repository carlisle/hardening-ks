##############################################################################
## el8-chrony
## Copyright (C) 2023 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/chrony.conf
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
## 
## Guide to the Secure Configuration of Red Hat Enterprise Linux 8
## https://static.open-scap.org/ssg-guides/ssg-rhel8-guide-index.html
##
## STIG RHEL8 Security Technical Implementation Guide 2022-12-06
## https://www.stigviewer.com/stig/red_hat_enterprise_linux_8/2022-12-06/
## 
## Enhacing security of RHEL8 Systems
## https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/
##
## CIS benchmarks
## https://www.cisecurity.org/cis-benchmarks/
##
## These scripts are inspired by various Benchmarks by The Center for Internet
## Security. However, these scripts have not been reviewed or approved by CIS
## and they do not guarantee that their use will result in compliance with the
## CIS baseline. 
##
## This benchmark is a direct port of a SCAP Security Guide benchmark developed
## for Red Hat Enterprise Linux. It has been modified through an automated
## process to remove specific dependencies on Red Hat Enterprise Linux and to
## function with any derivative of RHEL8. The result is a generally useful SCAP
## Security Guide benchmark.
##
## Derivatives of RHEL8 are not exact copies of Red Hat Enterprise Linux. There
## may be configuration differences that produce false positives and/or false
## negatives. If this occurs please file a bug report.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this guidance
## assume no responsibility whatsoever for its use by other parties, and makes
## no guarantees, expressed or implied, about its quality, reliability, or any
## other characteristic.
##
##############################################################################
## Notes
##
##############################################################################


# System timezone
# A remote time server for Chrony is configured
# CIS 2.1.2, CUI,
#timezone America/New_York --isUtc --ntpservers=0.us.pool.ntp.org,1.us.pool.ntp.org


%packages

# The Chrony package is installed
# CIS 2.1.1, CUI
chrony

%end

%post --log=/root/el8-chrony.log

#timestamp
echo "** el8-chrony START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/chrony/
NTPSOURCE1="0.us.pool.ntp.org"
NTPSOURCE2="1.us.pool.ntp.org"
NTPSOURCE3="2.us.pool.ntp.org"

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

/bin/cp -fpd /etc/chrony.conf ${BACKUPDIR}/chrony.conf-DEFAULT

####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

/bin/cp -fpd /etc/chrony.conf ${BACKUPDIR}/chrony.conf


## Disable chrony daemon from acting as server
## prevents daemon from listening on any port
PORT="\n# Disable chrony daemon from acting as server\n# CUI\nport 0\n"
printf "${PORT}" >> ${BACKUPDIR}/chrony.conf


## Disable network management of chrony daemon
## prevents daemon from listening for management connections
CMDPORT="\n# Disable network management of chrony daemon\n# CUI\ncmdport 0\n"
printf "${CMDPORT}" >> ${BACKUPDIR}/chrony.conf

####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

cat > ${BACKUPDIR}/postinstall-chrony.txt << 'EOF'

## Configure Time Service Maxpoll Interval
## continuously poll time servers
## STIG
## edit /etc/chrony.conf
## server|pool|peer <server> iburst maxpoll 16

systemctl restart chronyd

EOF

cat > ${BACKUPDIR}/admin_notes-chrony.txt << 'EOF'

# https://www.linuxtechi.com/sync-time-in-linux-server-using-chrony/
# https://stackoverflow.com/questions/49730407/how-to-resynchronize-with-chrony
# https://opensource.com/article/18/12/manage-ntp-chrony

# show timezones
timedatectl list-timezones

# set timezone
timedatectl set-timezone TIMEZONE

# resynchronize chrony
chronyc -a makestep

# testing
chronyd -q  'pool 0.us.pool.ntp.org iburst'

watch chronyc tracking

# monitor
chronyc activity

# show timesource servers
chronyc sources
chronyc sources -v
chronyc sourcestats -v

# verify
chronyc tracking

####
# configure chronyd server
IPRANGE="192.168.0.0"
PREFIX="16"

sed -i "s/#local stratum 10/local stratum 10/g" /etc/chrony.conf
sed -i "s/#allow 192.168.0.0\/16/allow ${IPRANGE}\/${PREFIX}/" /etc/chrony.conf
systemctl restart chronyd ; watch chronyc tracking

EOF



#####################
## DEPLOY NEW FILES
#####################

/bin/cp -f ${BACKUPDIR}/chrony.conf /etc/chrony.conf
/bin/chown root:root /etc/chrony.conf
/bin/chmod       644 /etc/chrony.conf

####################
## TURN ON SERVICE
####################

systemctl enable chronyd.service

#timestamp
echo "** el8-chrony COMPLETE" $(date +%F-%H%M-%S)

%end
