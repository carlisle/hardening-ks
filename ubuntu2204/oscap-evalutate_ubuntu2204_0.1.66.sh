#!/usr/bin/env bash

# References
# https://static.open-scap.org/ssg-guides/ssg-ubuntu2204-guide-index.html
# https://askubuntu.com/questions/1434977/installing-scap-security-guide-for-22-04-1
# https://avleonov.com/2022/10/04/how-to-perform-a-free-ubuntu-vulnerability-scan-with-openscap-and-canonicals-official-oval-content/
# https://idroot.us/install-openscap-ubuntu-22-04/
# https://askubuntu.com/questions/1446929/cis-based-compliance-check-for-ubuntu-22-04-using-openscap
# https://ubuntuforums.org/showthread.php?t=2479917
# https://ubuntu.com/security/certifications/docs/2204/usg/cis
# https://ubuntu.com/security/certifications/docs/2204/disa-stig/installation

# Evaluate all profiles, Ubuntu 22.04

HOST=$(hostname)
TIMESTAMP=$(date +%F-%H%M)
VERSION=0.1.66
LABEL=${HOST}-${TIMESTAMP}-${SCAPVER}

TARGETDIR=/root/oscap_results

OS=ubuntu2204
OSNAME=$(lsb_release -cs)


# Manual usage:
# Set the above varibles
# if [ ! -d "${TARGETDIR}" ]; then mkdir -p ${TARGETDIR}; fi
# cd ${TARGETDIR}
#
# curl -O https://bitbucket.org/carlisle/hardening-ks/raw/master/${OS}/oscap-evaluate_${OS}_${SCAPVER}.sh
# chmod 700 oscap-evaluate_${OS}_${SCAPVER}.sh
# dnf install scap-security-guide tar bzip2
# ionice -c2 -n7 nice -n 18 ./oscap-evaluate_${OS}_${SCAPVER}.sh &> scap-${LABEL}.log &


CONTENT=${TARGETDIR}/scap-security-guide-${VERSION}

# If using scap security guide provided by rpm, use this directory:
#CONTENT=/usr/share/xml/scap/ssg/content

# Download 

wget https://security-metadata.canonical.com/oval/com.ubuntu.${OSNAME}.usn.oval.xml.bz2

bunzip2 com.ubuntu.${OSNAME}.usn.oval.xml.bz2


# Download security guide

#curl -O https://github.com/ComplianceAsCode/content/releases/download/v${VERSION}/scap-security-guide-${VERSION}.tar.bz2
#curl -O https://github.com/ComplianceAsCode/content/releases/download/v${VERSION}/scap-security-guide-${VERSION}.tar.bz2.sha512

wget https://github.com/ComplianceAsCode/content/releases/download/v${VERSION}/scap-security-guide-${VERSION}.tar.bz2
wget https://github.com/ComplianceAsCode/content/releases/download/v${VERSION}/scap-security-guide-${VERSION}.tar.bz2.sha512

sha512sum --check scap-security-guide-${VERSION}.tar.bz2.sha512

tar xf scap-security-guide-${VERSION}.tar.bz2

# oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_cis_level1_server      /usr/share/xml/scap/ssg/content/ssg-ubuntu2204-ds.xml
# oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_cis_level1_workstation /usr/share/xml/scap/ssg/content/ssg-ubuntu2204-ds.xml
# oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_cis_level2_server      /usr/share/xml/scap/ssg/content/ssg-ubuntu2204-ds.xml
# oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_cis_level2_workstation /usr/share/xml/scap/ssg/content/ssg-ubuntu2204-ds.xml
# oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_standard               /usr/share/xml/scap/ssg/content/ssg-ubuntu2204-ds.xml


# To extract the list of profiles:
#oscap info ${CONTENT}/ssg-${OS}-ds.xml | grep profile | cut -d '_' -f 4

PARRAY=(

# Standard System Security Profile
    standard

cis_level1_server
cis_level1_workstation
cis_level2_server
cis_level2_workstation

       )

for PROFILE in "${PARRAY[@]}"; do 

	printf "\n#### %s ####\n\n" ${PROFILE}

	# Evaluate each profile against oval downloaded from RedHat

	oscap xccdf eval --fetch-remote-resources  --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
	 --results ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.xml \
	 --report  ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.html \
	 ${CONTENT}/ssg-${OS}-ds.xml;

	# Generate remediation script for each profile

	oscap xccdf generate fix --template urn:xccdf:fix:script:sh \
	 --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
	 --output ${TARGETDIR}/remediation-${HOST}-${DATE}-${PROFILE}.sh \
	 ${CONTENT}/ssg-${OS}-ds.xml;

        # Generate Guide for each profile 

#       oscap xccdf generate guide --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
#        --output ${TARGETDIR}/guide-${VERSION}-${PROFILE}.html \
#        ${CONTENT}/ssg-${OS}-ds.xml;

done

tar czf ${LABEL}.tar.gz oscap-evaluate*.sh ${LABEL}-*.xml ${LABEL}-*.html remediation-${LABEL}-*.sh guide-${LABEL}-*.html

