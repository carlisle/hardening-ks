##############################################################################
## rl8-openssl
## Copyright (C) 2023 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/profile.d/openssl-rand.sh
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## Guide to the Secure Configuration of Red Hat Enterprise Linux 8
## https://static.open-scap.org/ssg-guides/ssg-rhel8-guide-index.html
##
## STIG Rocky Linux 8
## https://docs.rockylinux.org/books/disa_stig/disa_stig_part1/
##
## STIG RHEL8 Security Technical Implementation Guide 2022-12-06
## https://www.stigviewer.com/stig/red_hat_enterprise_linux_8/2022-12-06/
## 
## Enhacing security of RHEL8 Systems
## https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/
##
## CIS Rocky Linux 8 benchmarks
## https://www.cisecurity.org/cis-benchmarks/
##
## These scripts are inspired by various Benchmarks by The Center for Internet
## Security. However, these scripts have not been reviewed or approved by CIS
## and they do not guarantee that their use will result in compliance with the
## CIS baseline. 
##
## This benchmark is a direct port of a SCAP Security Guide benchmark developed
## for Red Hat Enterprise Linux. It has been modified through an automated
## process to remove specific dependencies on Red Hat Enterprise Linux and to
## function with Rocky Linux. The result is a generally useful SCAP Security
## Guide benchmark with the following caveats:
##
## Rocky Linux is not an exact copy of Red Hat Enterprise Linux. There may be
## configuration differences that produce false positives and/or false
## negatives. If this occurs please file a bug report.
##
## Rocky Linux is a Linux distribution produced by the Rocky Enterprise Software
## Foundation. It is a free and open source operating system based on Red Hat
## Enterprise Linux with the goal "to be 100% bug-for-bug compatible" with its
## upstream commercial distribution. Rocky Linux has its own build system,
## compiler options, patchsets, and is a community supported, non-commercial
## operating system. Rocky Linux does not inherit certifications or evaluations
## from Red Hat Enterprise Linux. As such, some configuration rules (such as
## those requiring FIPS 140-2 encryption) will continue to fail on Rocky Linux.
##
## Members of the Rocky Linux community are invited to participate in OpenSCAP
## and SCAP Security Guide development. Bug reports and patches can be sent to
## GitHub: https://github.com/ComplianceAsCode/content. The mailing list is at
## https://fedorahosted.org/mailman/listinfo/scap-security-guide.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this guidance
## assume no responsibility whatsoever for its use by other parties, and makes
## no guarantees, expressed or implied, about its quality, reliability, or any
## other characteristic.
##
##############################################################################
## Notes
##
##############################################################################

%packages

crypto-policies
rng-tools
openssl

%end

%post --log=/root/rl8-openssl.log

#timestamp
echo "** rl8-openssl START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/openssl

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi


####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

cat > ${BACKUPDIR}/openssl-rand.sh << 'EOF'
## OpenSSL uses strong entropy source
## CUI, STIG
## This rule ensures that openssl invocations always uses SP800-90A compliant
## random number generator as a default behavior.
# provide a default -rand /dev/random option to openssl commands that
# support it

# written inefficiently for maximum shell compatibility
openssl()
(
  openssl_bin=/usr/bin/openssl

  case "$*" in
    # if user specified -rand, honor it
    *\ -rand\ *|*\ -help*) exec $openssl_bin "$@" ;;
  esac

  cmds=`$openssl_bin list -digest-commands -cipher-commands | tr '\n' ' '`
  for i in `$openssl_bin list -commands`; do
    if $openssl_bin list -options "$i" | grep -q '^rand '; then
      cmds=" $i $cmds"
    fi
  done

  case "$cmds" in
    *\ "$1"\ *)
      cmd="$1"; shift
      exec $openssl_bin "$cmd" -rand /dev/random "$@" ;;
  esac

  exec $openssl_bin "$@"
)
EOF

#####################
## DEPLOY NEW FILES
#####################

/bin/cp -f ${BACKUPDIR}/openssl-rand.sh /etc/profile.d/openssl-rand.sh
/bin/chown root:root                    /etc/profile.d/openssl-rand.sh
/bin/chmod       644                    /etc/profile.d/openssl-rand.sh

#timestamp
echo "** rl8-openssl COMPLETE" $(date +%F-%H%M-%S)

%end
