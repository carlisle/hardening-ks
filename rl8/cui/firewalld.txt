## There are three firewall management tools: iptables, firewalld, and nftables
## Make sure only one is running.

## For firewalld, make sure it is running and iptables and nftables are not

systemctl status firewalld iptables nftables

# if not, stop and disable iptables or nftables and enable and start firewalld

systemctl stop    iptables
systemctl disable iptables
systemctl stop    nftables
systemctl disable nftables
systemctl enabled firewalld
systemctl start   firewalld


## Show all firewalld rules

firewall-cmd --list-all-zones


## Log all denied packetts

# https://www.cyberciti.biz/faq/enable-firewalld-logging-for-denied-packets-on-linux/
# edit /etc/firewalld/firewalld.conf
LogDenied=all

# When this is done, log messages will show up in /var/log/messages with "kernel: filter_IN_<zone>_REJECT"

# Security, AllowZoneDrifting should be disabled (research this more)
edit /etc/firewalld/firewalld.conf


## Security 
# edit /etc/firewalld/firewalld.conf

# Set Default firewalld Zone for Incoming Packets
# xccdf_org.ssgproject.content_rule_set_firewalld_default_zone
# CIS 3.4.2.1
DefaultZone=drop

# If using ssh for remote connections, when the default zone is set to drop,
# assign the interface for the connection to a zone that allows ssh such as:
# external, public, home, or work, like this:
firewall-cmd --zone=drop --remove-interface=ens3
firewall-cmd --zone=public  --add-interface=ens3

firewall-cmd --zone=drop --remove-interface=ens3 --permanent
firewall-cmd --zone=public  --add-interface=ens3 --permanent

vi /etc/firewalld/firewalld.conf
DefaultZone=public
DefaultZone=drop

# Verify current state
firewall-cmd --state

firewall-cmd --list-all-zones
firewall-cmd --get-default-zone
firewall-cmd --get-active-zones
firewall-cmd --list-all
firewall-cmd --get-zones
firewall-cmd --get-services


firewall-cmd --set-default-zone=drop

# if you want your active network interface to be in the public zone
# allows ssh
firewall-cmd --zone=public --change-interface=ens3

firewall-cmd --get-default-zone

#test

# set changes to permanent
firewall-cmd --runtime-to-permanent

# Creating custom zone
firewall-cmd --permanent --new-zone=justssh
firewall-cmd --reload
firewall-cmd --zone=justssh --add-service=ssh
firewall-cmd --zone=justssh --change-interface=ens3
firewall-cmd --runtime-to-permanent
firewall-cmd --reload
firewall-cmd --get-active-zones
firewall-cmd --zone=justssh --list-services




## Network Address Translation / IP Masquerading

# In this example there are two interfaces: ens3 connects to the rest of the internet and ens10 on the internal network

# To enable nat, move the external interface, ens3 to the external zone.
# This is automatically configured for nat and network forwarding:

firewall-cmd --zone=public   --remove-interface=ens3 
firewall-cmd --zone=external --add-interface=ens3

# To open all ports on the internal interface, ens10, add it to the trusted zone
# This is not recommended but can be necessary when testing.

firewall-cmd --zone=public  --remove-interface=ens10
firewall-cmd --zone=trusted --add-interface=ens10


# If those are working correctly, make the changes permanent

firewall-cmd --zone=public --remove-interface=ens3  --permanent
firewall-cmd --zone=external  --add-interface=ens3  --permanent
firewall-cmd --zone=public --remove-interface=ens10 --permanent
firewall-cmd --zone=trusted   --add-interface=ens10 --permanent

# verify ip masquerading is enabled for external zone
firewall-cmd --zone=external --add-masquerade


# Verify that kernel forwarding is enabled
cat /proc/sys/net/ipv4/ip_forward

# To permanently enable kernel forwarding create file ( is this still needed with firewalld? )
/etc/sysctl.d/ip_forward.conf

net.ipv4.ip_forward = 1

# apply changes immediately:

sysctl -p


