##############################################################################
## rl8-aide
## Copyright (C) 2023 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/CONFIG.conf
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## STIG Rocky Linux 8
## https://docs.rockylinux.org/books/disa_stig/disa_stig_part1/
##
## STIG RHEL8 Security Technical Implementation Guide 2022-12-06
## https://www.stigviewer.com/stig/red_hat_enterprise_linux_8/2022-12-06/
## 
## Enhacing security of RHEL8 Systems
## https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/
##
## CIS Rocky Linux 8 benchmarks
## https://www.cisecurity.org/cis-benchmarks/
##
## These scripts are inspired by various Benchmarks by The Center for 
## Internet Security. However, these scripts have not been reviewed or 
## approved by CIS and they do not guarantee that their use will result 
## in compliance with the CIS baseline. 
##
## Rocky Linux is not an exact copy of Red Hat Enterprise Linux. There may be 
## configuration differences that produce false positives and/or false 
## negatives. If this occurs please file a bug report. Rocky Linux has its own 
## build system, compiler options, patchsets, and is a community supported,
## non-commercial operating system. Rocky Linux does not inherit certifications 
## or evaluations from Red Hat Enterprise Linux. As such, some configuration
## rules (such as those requiring FIPS 140-2 encryption) will continue to fail
## on Rocky Linux. Members of the Rocky Linux community are invited to
## participate in OpenSCAP and SCAP Security Guide development.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this 
## guidance assume no responsibility whatsoever for its use by other parties,
## and makes no guarantees, expressed or implied, about its quality, 
## reliability, or any other characteristic.
##
##############################################################################
## Notes
## Modify this template with: sed -i.bak s#rl8-aide#rl8-newfile#g rl8-newfile.cfg
##
##############################################################################

%packages

# Install AIDE
# CIS 1.3.1

aide
coreutils
crontabs

%end

%post --log=/root/rl8-aide.log

#timestamp
echo "** rl8-aide START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/aide

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

/bin/cp -fpd /etc/aide.conf ${BACKUPDIR}/aide.conf-DEFAULT
/bin/cp -fpd /etc/crontab   ${BACKUPDIR}/crontab-DEFAULT

####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.


cat > ${BACKUPDIR}/aide-crontab << 'EOFCRON1'
05 4 * * 0 root /usr/sbin/aide --check
EOFCRON1


############################
## /etc/cron.d/aide-check
############################

cat > ${BACKUPDIR}/aide-check << 'EOFCRON'
## CCE-26952-2 Configure Periodic Execution of AIDE 
## CCE-80374-2 Configure Notification of Post-AIDE Scan Details
05 4 * * * root /usr/sbin/aide --check | /bin/mail -s "$(hostname) - AIDE Integrity Check" root@localhost
EOFCRON

#############################
## postinstall-aide-check.txt
#############################

cat > ${BACKUPDIR}/postinstall-aide-check.txt<< 'EOFCHECK'
## CCE-26952-2 Configure Periodic Execution of AIDE 

# alternative ways to periodically execute aide
# pick just one

# the one used by default in this script
# in /etc/cron.d
echo "05 4 * * * root /usr/sbin/aide --check | /bin/mail -s "$(hostname) - AIDE Integrity Check" root@localhost" >> /etc/cron.d/aide-check

# the one used in the openscap remediation scripts
# in /etc/crontab
echo "05 4 * * * root /usr/sbin/aide --check | /bin/mail -s "$(hostname) - AIDE Integrity Check" root@localhost" >> /etc/crontab

# in root's crontab: crontab
05 4 * * * /usr/sbin/aide --check | /bin/mail -s "$(hostname) - AIDE Integrity Check" root@localhost
EOFCHECK


#############################
## postinstall-aide-update.txt
#############################

cat > ${BACKUPDIR}/postinstall-aide-update.txt << 'EOFUP'

umask 0077
cd /var/lib/aide/
aide --update > aide-$(date +%F).log 2> aide-$(date +%F).log &

umask 0077
cd /var/lib/aide/
/bin/cp -fpd aide.db.new.gz aide.db.gz
/bin/cp -ipd aide.db.new.gz aide-$(date +%F).db.gz
chmod 400 aide-$(date +%F).db.gz
sha256sum aide-$(date +%F).db.gz >> aide.sha256.txt

#off server backup of /var/lib/aide/*

EOFUP

#####################
## DEPLOY NEW FILES
#####################

cat ${BACKUPDIR}/aide-crontab >> /etc/crontab

#/bin/cp -f ${BACKUPDIR}/aide-check /etc/cron.d/aide-check
#/bin/chown root:root /etc/cron.d/aide-check
#/bin/chmod       600 /etc/cron.d/aide-check

# Build and Test AIDE Database
# CIS 1.3.1
#############################################
#  Running this will noticably increase the time to install 
#  the operating system.
#############################################  

/sbin/aide --init

## MOVE DATABASE

/bin/cp -fpd /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz
/bin/chown root:root /var/lib/aide/aide.db.gz
/bin/chmod       600 /var/lib/aide/aide.db.gz
/bin/cp -ipd /var/lib/aide/aide.db.gz /var/lib/aide/aide-$(date +%F).db.gz
/bin/chmod       400 /var/lib/aide/aide-$(date +%F).db.gz
/bin/sha256sum /var/lib/aide/aide-$(date +%F).db.gz >> /var/lib/aide/aide.sha256.txt

## CHECK DATABASE

#/usr/sbin/aide --check


####################
## TURN ON SERVICE
####################

systemctl enable crond

#timestamp
echo "** rl8-aide COMPLETE" $(date +%F-%H%M-%S)

%end
