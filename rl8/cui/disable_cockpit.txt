
systemctl stop cockpit
systemctl disable cockpit

dnf remove cockpit cockpit-ws

# remove hole in firewall for cockpit
firewall-cmd --list-all

firewall-cmd --permanent --remove-service=cockpit

systemctl restart firewalld
firewall-cmd --list-all
