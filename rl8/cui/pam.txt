
# /etc/pam.d/
# /etc/security
# /etc/authselect/

### Protect Accounts by Configuring PAM

## Enable authselect
## CIS 1.2.3
## Description: Configure user authentication setup to use the authselect tool.
## If authselect profile is selected, the rule will enable the sssd profile.
## Rationale: Authselect is a successor to authconfig. It is a tool to select
## system authentication and identity sources from a list of supported profiles
## instead of letting the administrator manually build the PAM stack. That way,
## it avoids potential breakage of configuration, as it ships several tested
## profiles that are well tested and supported to solve different use-cases.
## Warnings: warning  If the sudo authselect select command returns an error
## informing that the chosen profile cannot be selected, it is probably because
## PAM files have already been modified by the administrator. If this is the
## case, in order to not overwrite the desired changes made by the
## administrator, the current PAM settings should be investigated before
## forcing the selection of the chosen authselect profile.
authselect select --force sssd

## Set Lockouts for Failed Password Attempts

# Limit Password Reuse: password-auth
# Limit Password Reuse: system-auth
# CIS 5.5.3
# On systems with newer versions of authselect, the pam_pwhistory PAM module
# can be enabled via authselect feature:
var_password_pam_remember='5'
var_password_pam_remember_control_flag='requisite,required'
authselect enable-feature with-pwhistory
authselect apply-changes -b

# Lock Accounts After Failed Password Attempts
# CIS 5.4.2
var_accounts_passwords_pam_faillock_deny='3'

## Protect Accounts by Restricting Password-Based Login



