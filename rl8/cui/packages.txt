

## Updating Software

# Rocky Linux key information
https://rockylinux.org/keys/

https://dl.rockylinux.org/pub/rocky/RPM-GPG-KEY-Rocky-8
https://dl.rockylinux.org/pub/rocky/RPM-GPG-KEY-Rocky-9
https://dl.rockylinux.org/pub/rocky/RPM-GPG-KEY-rockyofficial

gpg --show-keys --with-fingerprint --with-colons /etc/pki/rpm-gpg/RPM-GPG-KEY-rockyofficial

# Ensure Red Hat GPG Key Installed
#rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release

# Ensure Rocky Linux GPG Key Installed
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-rockyofficial


rpm -qa gpg-pubkey*
# for each key shown above, do rpm -qi <key>



# Install subscription-manager Package
dnf install subscription-manager

# Install dnf-plugin-subscription-manager Package
dnf install dnf-plugin-subscription-manager


# Ensure gpgcheck Enabled In Main yum Configuration
# CIS 1.2.3, CUI
# verify set to 1
# grep "gpgcheck" /etc/dnf/dnf.conf

# Ensure gpgcheck Enabled for Local Packages
# verify set to 1
# grep "localpkg_gpgcheck" /etc/dnf/dnf.conf

# Ensure gpgcheck Enabled for All yum Package Repositories
# verify none is set to 0
# grep "gpgcheck" /etc/yum.repos.d/*


# Install dnf-automatic Package
dnf install dnf-automatic

# Configure dnf-automatic to Install Available Updates Automatically
edit /etc/dnf/automatic.conf
add "apply_updates = yes" under [commands] section

# Configure dnf-automatic to Install Only Security Updates
edit /etc/dnf/automatic.conf
add "upgrade_type = security" under [commands] section

# Enable dnf-automatic Timer
systemctl enable dnf-automatic.timer



