##############################################################################
## rl8-fstab
## Copyright (C) 2023 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/fstab
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## STIG Rocky Linux 8
## https://docs.rockylinux.org/books/disa_stig/disa_stig_part1/
##
## STIG RHEL8 Security Technical Implementation Guide 2022-12-06
## https://www.stigviewer.com/stig/red_hat_enterprise_linux_8/2022-12-06/
## 
## Enhacing security of RHEL8 Systems
## https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/
##
## CIS Rocky Linux 8 benchmarks
## https://www.cisecurity.org/cis-benchmarks/
##
## These scripts are inspired by various Benchmarks by The Center for 
## Internet Security. However, these scripts have not been reviewed or 
## approved by CIS and they do not guarantee that their use will result 
## in compliance with the CIS baseline. 
##
## Rocky Linux is not an exact copy of Red Hat Enterprise Linux. There may be 
## configuration differences that produce false positives and/or false 
## negatives. If this occurs please file a bug report. Rocky Linux has its own 
## build system, compiler options, patchsets, and is a community supported,
## non-commercial operating system. Rocky Linux does not inherit certifications 
## or evaluations from Red Hat Enterprise Linux. As such, some configuration
## rules (such as those requiring FIPS 140-2 encryption) will continue to fail
## on Rocky Linux. Members of the Rocky Linux community are invited to
## participate in OpenSCAP and SCAP Security Guide development.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this 
## guidance assume no responsibility whatsoever for its use by other parties,
## and makes no guarantees, expressed or implied, about its quality, 
## reliability, or any other characteristic.
##
##############################################################################
## Notes
##
##############################################################################

%packages

setup

%end

%post --log=/root/rl8-fstab.log

#timestamp
echo "** rl8-fstab START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/fstab

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

/bin/cp -fpd /etc/fstab ${BACKUPDIR}/fstab-DEFAULT
/bin/cp -fpd /etc/fstab ${BACKUPDIR}/fstab

####################
## WRITE NEW FILES
####################

echo "#"                                                       >> ${BACKUPDIR}/fstab
echo "### CCE-80152-2 CCE-80154-8 CCE-80153-0"                 >> ${BACKUPDIR}/fstab 
echo "### Add nodev, nosuid, noexec to /dev/shm"               >> ${BACKUPDIR}/fstab
echo "shmfs  /dev/shm  tmpfs  nodev,nosuid,noexec         0 0" >> ${BACKUPDIR}/fstab

#####################
## DEPLOY NEW FILES
#####################

/bin/cp -f ${BACKUPDIR}/fstab /etc/fstab
/bin/chown root:root /etc/fstab
/bin/chmod 644       /etc/fstab

#timestamp
echo "** rl8-fstab COMPLETE" $(date +%F-%H%M-%S)

%end
