##############################################################################
## rl8-pam
## Copyright (C) 2023 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/security/
## /etc/default/useradd
## /etc/login.defs
## /etc/pam.d/
## /etc/authselect/
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## Guide to the Secure Configuration of Red Hat Enterprise Linux 8
## https://static.open-scap.org/ssg-guides/ssg-rhel8-guide-index.html
##
## STIG Rocky Linux 8
## https://docs.rockylinux.org/books/disa_stig/disa_stig_part1/
##
## STIG RHEL8 Security Technical Implementation Guide 2022-12-06
## https://www.stigviewer.com/stig/red_hat_enterprise_linux_8/2022-12-06/
## 
## Enhacing security of RHEL8 Systems
## https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/
##
## CIS Rocky Linux 8 benchmarks
## https://www.cisecurity.org/cis-benchmarks/
##
## These scripts are inspired by various Benchmarks by The Center for 
## Internet Security. However, these scripts have not been reviewed or 
## approved by CIS and they do not guarantee that their use will result 
## in compliance with the CIS baseline. 
##
## Rocky Linux is not an exact copy of Red Hat Enterprise Linux. There may be 
## configuration differences that produce false positives and/or false 
## negatives. If this occurs please file a bug report. Rocky Linux has its own 
## build system, compiler options, patchsets, and is a community supported,
## non-commercial operating system. Rocky Linux does not inherit certifications 
## or evaluations from Red Hat Enterprise Linux. As such, some configuration
## rules (such as those requiring FIPS 140-2 encryption) will continue to fail
## on Rocky Linux. Members of the Rocky Linux community are invited to
## participate in OpenSCAP and SCAP Security Guide development.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this 
## guidance assume no responsibility whatsoever for its use by other parties,
## and makes no guarantees, expressed or implied, about its quality, 
## reliability, or any other characteristic.
##
##############################################################################
## Notes
##
##############################################################################

%packages

pam
libpwquality
shadow-utils
authselect-libs

%end

%post --log=/root/rl8-pam.log

#timestamp
echo "** rl8-pam START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/pam

## Settings for /etc/login.defs
# Maximum age of password for new accounts
PW_PASS_MAX_DAYS="365"
# Minimum age of password for new accounts
PW_PASS_MIN_DAYS="7"
# Issue warning to uses this number of days before password expires
PW_PASS_WARN_AGE="7"

## Settings for /etc/security/pwquality.conf
# Character type minimum requirements from Upper-case, Lower-case, Numbers, Special characters
PW_MINCLASS="4"
# Password Length minimum
PW_MINLEN="14"
# Number of retry prompts per session
PW_RETRY="3"

## Settings for /etc/security/pwhistory.conf
## No not allow use of previous passwords
## Must be 5 or more
PW_REMEMBER="5"

## Settings for /etc/security/faillock.conf
## After this number of failed loging attempts, lockout the account
PW_DENY="3" 		# maybe this should be set to PW_RETRY
## Disable account for this number of seconds
PW_LOCKOUT="900"

## Settings for /etc/default/usermod
# Disable account this number of days after password expires
PW_INACTIVE="30"

#PW_UP=1
#PW_LOW=1
#PW_NUM=1
#PW_OTH=1

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

/bin/cp -Rfpd /etc/security/   ${BACKUPDIR}/security-DEFAULT/
/bin/cp -Rfpd /etc/pam.d/      ${BACKUPDIR}/pam.d-DEFAULT/
/bin/cp -Rfpd /etc/authselect/ ${BACKUPDIR}/authselect-DEFAULT/
/bin/cp -Rfpd /etc/default/    ${BACKUPDIR}/default-DEFAULT/
/bin/cp  -fpd /etc/login.defs  ${BACKUPDIR}/login.defs-DEFAULT

# Enable authselect
# CIS 1.2.3
authselect select --force sssd


####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

###################
# /etc/login.defs
# Controls parameters used when creating new accounts
###################
/bin/cp  -fpd ${BACKUPDIR}/login.defs-DEFAULT ${BACKUPDIR}/login.defs

OLDDATA="^PASS_MAX_DAYS	99999"
NEWDATA="# Set Password Maximum Age\n# CIS 5.6.1.1\nPASS_MAX_DAYS\t${PW_PASS_MAX_DAYS}\n"
sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/login.defs

OLDDATA="^PASS_MIN_DAYS	0"
NEWDATA="\n# Set Password Minimum Age\n# CIS 5.6.1.2\nPASS_MIN_DAYS\t${PW_PASS_MIN_DAYS}\n"
sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/login.defs

OLDDATA="^PASS_WARN_AGE   7"
NEWDATA="\n# Set Password Warning Age\n# CIS 5.6.1.3\nPASS_WARN_AGE\t${PW_PASS_WARN_AGE}\n"
sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/login.defs

cat > ${BACKUPDIR}/postinstall-set-password-age.txt << EOFPWAGE

# generate list of accounts
MINUID=$(awk '/^UID_MIN/ {print $2}' /etc/login.defs)
MAXUID=$(awk '/^UID_MAX/ {print $2}' /etc/login.defs)
eval getent passwd {${MINUID}..${MAXUID}} | cut -d: -f1

# Set Existing Passwords Maximum Age
chage -M ${PW_PASS_MAX_DAYS} <account>
# Set Existing Passwords Minimum Age
chage -m ${PW_PASS_MIN_DAYS} <account>

EOFPWAGE


################################
# /etc/security/pwquality.conf
################################
/bin/cp  -fpd ${BACKUPDIR}/security-DEFAULT/pwquality.conf ${BACKUPDIR}/pwquality.conf

OLDDATA="^# minclass = 0"
NEWDATA="# Ensure PAM Enforces Password Requirements - Minimum Different Categories\n# CIS 5.5.1\nminclass = ${PW_MINCLASS}\n"
sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/pwquality.conf

OLDDATA="^# minlen = 8"
NEWDATA="# Ensure PAM Enforces Password Requirements - Minimum Length\n# CIS 5.5.1\nminlen = ${PW_MINLEN}\n"
sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/pwquality.conf

OLDDATA="^# retry = 3"
NEWDATA="\n# Ensure PAM Enforces Password Requirements - Authentication Retry Prompts Permitted Per-Session\n# CIS 5.4.1\nretry = ${PW_RETRY}\n"
sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/pwquality.conf

################################
# /etc/security/pwhistory.conf
################################

authselect enable-feature with-pwhistory

/bin/cp  -fpd ${BACKUPDIR}/security-DEFAULT/pwhistory.conf ${BACKUPDIR}/pwhistory.conf

OLDDATA="^# remember = 10"
NEWDATA="# Limit Password Reuse: password-auth and system-auth\n# CIS 5.5.3\nremember = ${PW_REMEMBER}\n"
sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/pwhistory.conf


################################
# /etc/security/faillock.conf
################################

authselect enable-feature with-faillock

/bin/cp  -fpd ${BACKUPDIR}/security-DEFAULT/faillock.conf ${BACKUPDIR}/faillock.conf

OLDDATA="^# deny = 3"
NEWDATA="# Lock Accounts After Failed Password Attempts\n# CIS 5.4.2\ndeny = ${PW_DENY}\n"
sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/faillock.conf

OLDDATA="^# unlock_time = 600"
NEWDATA="# Set Lockout Time for Failed Password Attempts - 15min\n# CIS 5.4.2\nunlock_time = ${PW_LOCKOUT}\n"
sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/faillock.conf

#########################
## /etc/default/useradd
#########################
/bin/cp  -fpd ${BACKUPDIR}/default-DEFAULT/useradd ${BACKUPDIR}/useradd

OLDDATA="^INACTIVE=-1"
NEWDATA="\n# Set Account Expiration Following Inactivity\n# CIS 5.6.1.4\nINACTIVE=${PW_INACTIVE}\n"
sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/useradd


#########################
## /etc/pam.d/su
#########################
/bin/cp  -fpd ${BACKUPDIR}/pam.d-DEFAULT/su ${BACKUPDIR}/su

OLDDATA="^#auth		required	pam_wheel.so use_uid"
NEWDATA="\n# Enforce usage of pam_wheel for su authentication\n# CIS 5.3.7\nauth\t\trequired\tpam_wheel.so use_uid\n"
sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/su

#####################
## DEPLOY NEW FILES
#####################

/bin/cp -f ${BACKUPDIR}/login.defs /etc/login.defs
/bin/chown root:root               /etc/login.defs
/bin/chmod       644               /etc/login.defs

/bin/cp -f ${BACKUPDIR}/pwquality.conf /etc/security/pwquality.conf
/bin/chown root:root                   /etc/security/pwquality.conf
/bin/chmod       644                   /etc/security/pwquality.conf

/bin/cp -f ${BACKUPDIR}/pwhistory.conf /etc/security/pwhistory.conf
/bin/chown root:root                   /etc/security/pwhistory.conf
/bin/chmod       644                   /etc/security/pwhistory.conf

/bin/cp -f ${BACKUPDIR}/faillock.conf /etc/security/faillock.conf
/bin/chown root:root                  /etc/security/faillock.conf
/bin/chmod       644                  /etc/security/faillock.conf

/bin/cp -f ${BACKUPDIR}/useradd /etc/default/useradd
/bin/chown root:root            /etc/default/useradd
/bin/chmod       644            /etc/default/useradd

/bin/cp -f ${BACKUPDIR}/su /etc/pam.d/su
/bin/chown root:root       /etc/pam.d/su
/bin/chmod       644       /etc/pam.d/su

authselect apply-changes -b

####################
## TURN ON SERVICE
####################

systemctl enable  sssd


#timestamp
echo "** rl8-pam COMPLETE" $(date +%F-%H%M-%S)

 %end
