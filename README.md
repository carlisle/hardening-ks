# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This repository contains a set of kickstart files that will implement 
security policies for a Centos 6 or 7 installation during install.
There are many different kickstart files divied by function
so that they can be included, excluded, or modified as needed.
  
The modified configuraion files are annotated to indicate 
the source of the security settings so that an administrator
can research the settings that are best for their situation.  

Two primary kickstart files are used to create virtual machines
that will demonstrate the use of the set of included kickstart 
files: base7-ks.cfg will create a minimal install of Centos 7 
with default security settings.  server7-ks.cfg will create a
virtual machine with most of the security recommenations. 
evaluate.sh will download and evalute the virtual machine 
against the set of all openscap security policies.  
Comparing the results of this script between 
the virtual machines created by base7-ks.cfg and 
server7-ks.cfg will provide an evaulation of these
kickstart files. The files base6-ks.cfg and server6-ks.cfg 
provide similar kickstart files for Centos 6.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
