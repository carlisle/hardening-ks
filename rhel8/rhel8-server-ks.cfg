##############################################################################
## rhel8-server-ks.cfg
## Copyright (C) 2022 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## OpenSCAP Security Guide to the Secure Configuration of 
## Red Hat Enterprise Linux 8
## http://static.open-scap.org/ssg-guides/ssg-centos8-guide-index.html
## http://static.open-scap.org/ssg-guides/ssg-rhel8-guide-index.html
##
## CCE = Common Configuration Enumeration
## https://nvd.nist.gov/cce/index.cfm
##
## Red Hat Enterprise Linux 8 Security Hardening
## https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/index
##
## CIS Red Hat Enterprise Linux 8 Benchmark v1.0.1
## https://www.cisecurity.org/cis-benchmarks/
##
## These scripts are inspired by various Benchmarks by The Center for 
## Internet Security. However, these scripts have not been reviewed or 
## approved by CIS and they do not guarantee that their use will result 
## in compliance with the CIS baseline. 
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this 
## guidance assume no responsibility whatsoever for its use by other parties,
## and makes no guarantees, expressed or implied, about its quality, 
## reliability, or any other characteristic.
##
##############################################################################
## Notes
##
## https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/performing_an_advanced_rhel_installation/starting-kickstart-installations_installing-rhel-as-an-experienced-user#starting-a-kickstart-installation-manually_starting-kickstart-installations
##
## https://docs.centos.org/en-US/centos/install-guide/Kickstart2/
##
## Usage: Create a virtual machine of at least 1 cpu, 4 GB Ram and
## a virtual disk of at least 20 GB in size using VirtIO driver
##
## vmlinuz initrd=initrd.img inst.ks=https://bitbucket.org/carlisle/hardening-ks/raw/master/rhel8/rhel8-server-ks.cfg
##
##############################################################################

#version=RHEL8.5

# Use graphical install
graphical
# Keyboard layouts
keyboard --xlayouts='us'
# System language
lang en_US.UTF-8
# Run the Setup Agent on first boot
firstboot --enable
# Do not configure the X Window System
skipx
# System timezone
timezone America/New_York --isUtc --ntpservers=0.us.pool.ntp.org,1.us.pool.ntp.org


# Network information
network  --bootproto=dhcp --device=ens3 --ipv6=auto --activate
network  --hostname=rhel8-server

# Use network installation
#url --url="https://mirrors.rit.edu/rocky/8/BaseOS/x86_64/os/"

#repo --name="rh8base"       --baseurl="https://mirrors.rit.edu/rocky/8/BaseOS/x86_64/os/"
#repo --name="rh8powertools" --baseurl="https://mirrors.rit.edu/rocky/8/PowerTools/x86_64/os/"
#repo --name="rh8extras"     --baseurl="https://mirrors.rit.edu/rocky/8/extras/x86_64/os/"
#repo --name="rh8apps"    --baseurl="https://mirrors.rit.edu/rocky/8/AppStream/x86_64/os/"


repo --name="epel8" --metalink="https://mirrors.fedoraproject.org/metalink?repo=epel-8&arch=$basearch&infra=$infra&content=$contentdir"


# Disk partitioning information
ignoredisk --only-use=vda
# Partition clearing information
clearpart --all --initlabel --drives=vda
zerombr

#autopart --type=lvm

# Disk partitioning information
part /boot  --fstype="ext4"  --ondisk=vda --size=1024  --label=boot
part pv.429 --fstype="lvmpv" --ondisk=vda --size=16860 --grow

volgroup rh8 --pesize=4096 pv.429

logvol /     --fstype="xfs"  --size=10240 --label="root" --name=root --vgname=rh8
logvol swap  --fstype="swap" --size=488                  --name=swap --vgname=rh8

logvol /home --fstype="xfs"  --size=2048  --label="home" --name=home --vgname=rh8
logvol /tmp  --fstype="xfs"  --size=488   --label="tmp"  --name=tmp  --vgname=rh8

logvol /var       --fstype="xfs" --size=2048 --label="var" --name=var --vgname=rh8
logvol /var/log     --fstype="xfs" --size=1024 --label="log" --name=log  --vgname=rh8
logvol /var/log/audit --fstype="xfs" --size=512 --label="audit" --name=audit --vgname=rh8


#Root password
rootpw --lock

# User accounts
user --name=admin --password=abc123 --plaintext --gecos="admin" --groups=wheel
user --name=user  --password=abc123 --plaintext --gecos="user"


%packages
@^minimal-environment

# no kdump
-kexec-tools

@core

#@standard

epel-release

policycoreutils-restorecond

psacct
#logwatch

### CCE-80187-8 Ensure rsyslog is installed
rsyslog
#rsyslog-gnutls
#rsyslog-relp

### CCE-26940-7 Install the screen Package
screen

### CCE-27626-1 Install openswan Package
libreswan

### CCE-80213-2 Uninstall tftp-server 
-tftp-server

## Minimum packages needed to run X programs remotely
##xorg-x11-xauth
##xterm

%end


%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end

# Reboot after installation
reboot


#################
## POST INSTALL
#################

%post --log=/root/rhel8-server-ks.cfg.log

cat /proc/cmdline > /root/install.ks-boot_parameters

if grep -i -q "para=[a-zA-Z0-9]" /proc/cmdline
then
    THIS_PARA=`cat /proc/cmdline | sed 's/.*para=\([^ ]*\).*/\1/'`
fi

#timestamp
echo "** rhel8-base-ks.cfg START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

/bin/cp -LRipd /etc ${BACKUPDIR}/etc-$(date +%F)-DEFAULT

#timestamp
echo "** rhel8-base-ks.cfg COMPLETE" $(date +%F-%H%M-%S)

%end


%include https://bitbucket.org/carlisle/hardening-ks/raw/master/rhel8/rh8-services.cfg
