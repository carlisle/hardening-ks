##############################################################################
## rhel8-base-ks.cfg
## Copyright (C) 2021 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/CONFIG.conf
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## OpenSCAP Security Guide to the Secure Configuration of 
## Red Hat Enterprise Linux 8
## http://static.open-scap.org/ssg-guides/ssg-centos8-guide-index.html
## http://static.open-scap.org/ssg-guides/ssg-rhel8-guide-index.html
##
## CCE = Common Configuration Enumeration
## https://nvd.nist.gov/cce/index.cfm
##
## Red Hat Enterprise Linux 8 Security Hardening
## https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/index
##
## CIS Red Hat Enterprise Linux 8 Benchmark v1.0.0
## https://www.cisecurity.org/cis-benchmarks/
##
## These scripts are inspired by various Benchmarks by The Center for 
## Internet Security. However, these scripts have not been reviewed or 
## approved by CIS and they do not guarantee that their use will result 
## in compliance with the CIS baseline. 
##
## Rocky Linux is not an exact copy of Red Hat Enterprise Linux. There may be 
## configuration differences that produce false positives and/or false 
## negatives. If this occurs please file a bug report. Rocky Linux has its own 
## build system, compiler options, patchsets, and is a community supported,
## non-commercial operating system. Rocky Linux does not inherit certifications 
## or evaluations from Red Hat Enterprise Linux. As such, some configuration
## rules (such as those requiring FIPS 140-2 encryption) will continue to fail
## on Rocky Linux. Members of the Rocky Linux community are invited to participate in 
## OpenSCAP and SCAP Security Guide development.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this 
## guidance assume no responsibility whatsoever for its use by other parties,
## and makes no guarantees, expressed or implied, about its quality, 
## reliability, or any other characteristic.
##
##############################################################################
## Notes
##
## https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/performing_an_advanced_rhel_installation/starting-kickstart-installations_installing-rhel-as-an-experienced-user#starting-a-kickstart-installation-manually_starting-kickstart-installations
##
## https://docs.centos.org/en-US/centos/install-guide/Kickstart2/
##
## Usage: Create a virtual machine of at least 1 cpu, 4 GB Ram and
## a virtual disk of at least 20 GB in size using VirtIO driver
##
## vmlinuz initrd=initrd.img inst.ks=https://bitbucket.org/carlisle/hardening-ks/raw/master/rhel8/rhel8-base-ks.cfg
##
##############################################################################

#version=RHEL8

# Use graphical install
graphical
# Keyboard layouts
keyboard --xlayouts='us'
# System language
lang en_US.UTF-8
# Run the Setup Agent on first boot
firstboot --enable
# Do not configure the X Window System
skipx
# System timezone
timezone America/New_York --isUtc --ntpservers=0.us.pool.ntp.org,1.us.pool.ntp.org


# Network information
network  --bootproto=dhcp --device=ens3 --ipv6=auto --activate
network  --hostname=base8

# Use network installation
#url --url="https://download.rockylinux.org/pub/rocky/8/BaseOS/x86_64/os/"

# Disk partitioning information
ignoredisk --only-use=vda
# Partition clearing information
clearpart --all --initlabel --drives=vda
zerombr

autopart --type=lvm


#Root password
rootpw --lock

# User accounts
user --name=admin --password=abc123 --plaintext --gecos="admin" --groups=wheel
user --name=user  --password=abc123 --plaintext --gecos="user"


%packages
@^minimal-environment
kexec-tools

%end


%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end

# Reboot after installation
reboot


#################
## POST INSTALL
#################

%post --log=/root/rhel8-base-ks.cfg.log

cat /proc/cmdline > /root/install.ks-boot_parameters

if grep -i -q "para=[a-zA-Z0-9]" /proc/cmdline
then
    THIS_PARA=`cat /proc/cmdline | sed 's/.*para=\([^ ]*\).*/\1/'`
fi

#timestamp
echo "** rhel8-base-ks.cfg START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

/bin/cp -LRipd /etc ${BACKUPDIR}/etc-$(date +%F)-DEFAULT

#timestamp
echo "** rhel8-base-ks.cfg COMPLETE" $(date +%F-%H%M-%S)

%end
