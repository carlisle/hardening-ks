#!/usr/bin/env bash

# This evaluates against SCAP 0.1.75 provided by the scap-security-guide-0.1.75-1 rpm
# Release Notes: https://github.com/OpenSCAP/scap-security-guide/releases/tag/v0.1.75
# OpenSCAP User Manual: https://static.open-scap.org/openscap-1.4.1/oscap_user_manual.html

# Evaluate all selected profiles

# Ensure packages are up to date
# dnf update openscap scap-security-guide

HOST=$(hostname)
TIMESTAMP=$(date +%F-%H%M)
TARGETDIR=/root/oscap_results

SCAPVER=0.1.75
# rpm -qa | grep scap-security-guide | cut -d '-' -f 4

OS=fedora
# cat /etc/os-release | grep PLATFORM_ID | cut -d '"' -f 2 | cut -d ':' -f 2

LABEL=${HOST}-${TIMESTAMP}-${SCAPVER}

# Use content from installed package
CONTENT=/usr/share/xml/scap/ssg/content

DATASTREAM="ssg-${OS}-ds.xml"


# Manual usage:
# Set the above varibles
# if [ ! -d "${TARGETDIR}" ]; then mkdir -p ${TARGETDIR}; fi
# cd ${TARGETDIR}
#
# curl -O https://bitbucket.org/carlisle/hardening-ks/raw/master/${OS}/oscap-evaluate_${OS}_${SCAPVER}.sh
# chmod 700 oscap-evaluate_${OS}_${SCAPVER}.sh
# dnf install scap-security-guide tar bzip2
# ionice -c2 -n7 nice -n 18 ./oscap-evaluate_${OS}_${SCAPVER}.sh &> scap-${LABEL}.log &
# ionice -c2 -n7 nice -n 18 \
#   ./oscap-evaluate_$(cat /etc/os-release | grep PLATFORM_ID | cut -d '"' -f 2 | cut -d ':' -f 2}_$(rpm -qa | grep scap-security-guide | cut -d '-' -f 4).sh \
#   &> scap-${LABEL}.log &


# To extract the list of profiles:
#oscap info ${CONTENT}/DATASTREAM | grep profile | sed 's+.*profile_++'

PARRAY=(
# Standard System Security Profile for Fedora
    standard
# Common User Security Profile for Fedora Workstation
    cusp_fedora
# United States Government Configuration Baseline (USGCB / STIG)
    ospp
# Payment Card Industry Data Security Standard   
    pci-dss
)


for PROFILE in "${PARRAY[@]}"; do
    printf "\n#### %s ####\n\n" ${PROFILE}

    # Evaluate each profile using installed datastream file
     oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
      --results ${TARGETDIR}/${LABEL}-${PROFILE}.xml \
      --report  ${TARGETDIR}/${LABEL}-${PROFILE}.html \
      ${CONTENT}/${DATASTREAM}

    # Generate remediation script for each profile
    oscap xccdf generate fix --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
      --output ${TARGETDIR}/remediation-${LABEL}-${PROFILE}.sh \
      ${CONTENT}/${DATASTREAM}

    # Generate Guide for each profile
    oscap xccdf generate guide --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
      --output ${TARGETDIR}/guide-${LABEL}-${PROFILE}.html \
      ${CONTENT}/${DATASTREAM}

done

tar czf ${LABEL}.tar.gz oscap-evaluate*  ${LABEL}-*.xml ${LABEL}-*.html remediation-${LABEL}-*.sh guide-${LABEL}-*.html

# END 
