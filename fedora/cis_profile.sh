# Generating cis profiles for Fedora

# Use a non-root account for doing this"
# Reference: https://forums.centos.org/viewtopic.php?t=77643

# install cmake
# is python3-jinja2 installed?

# Download scap 0.1.66 content

wget https://github.com/ComplianceAsCode/content/archive/refs/tags/v0.1.66.tar.gz

tar xf v0.1.66.tar.gz

cd content-0.1.66

sed -i $'s/standard_profiles =.*/standard_profiles = [ \
    \'standard\', \
    \'cis\', \
    \'cis_server_l1\', \
    \'cis_workstation_l1\', \
    \'cis_workstation_l2\', \
    \'pci-dss\', \
    \'C2S\', \
    \'anssi_nt28_enhanced\', \
    \'anssi_nt28_high\', \
    \'anssi_nt28_intermediary\', \
    \'anssi_nt28_minimal\', \
    \'cjis\', \
    \'cui\', \
    \'e8\', \
    \'hipaa\', \
    \'ncp\', \
    \'ospp\', \
    \'rhelh-stig\', \
    \'rhelh-vpp\', \
    \'rht-ccp\', \
    \'stig\', \
    \'stig_gui\' \
]/' ssg/constants.py

# this will build profiles for fedora 
./build_product --derivatives fedora

sed -i "/^standard_profiles =/s/]/, 'cis_workstation_l2']/" ssg/constants.py

sed -Ei 's|(/boot/efi/EFI/)redhat/|\1fedora/|g' build/ssg-fedora-ds*.xml


oscap info build/ssg-fedora-ds.xml


CONTENT=/blah/content-0.1.63/build/
