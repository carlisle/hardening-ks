##############################################################################
## c7-oscap-none.cfg
## Copyright (C) 2023 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /root/oscap_results/*
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## OpenSCAP Security Guide to the Secure Configuration of 
## Red Hat Enterprise Linux 7
## http://static.open-scap.org/ssg-guides/ssg-centos7-guide-index.html
## http://static.open-scap.org/ssg-guides/ssg-rhel7-guide-index.html
## http://people.redhat.com/swells/scap-security-guide/RHEL/7/output/table-rhel7-cces.html
##
## CCE = Common Configuration Enumeration
## https://nvd.nist.gov/cce/index.cfm
##
## Red Hat Enterprise Linux 7 Security Guide
## https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/
##
## CIS CentOS_Linux_7_Benchmark_v1.1.0
## https://benchmarks.cisecurity.org/tools2/linux/CIS_CentOS_Linux_7_Benchmark_v1.1.0.pdf
##
## These scripts are inspired by various Benchmarks by The Center for 
## Internet Security. However, these scripts have not been reviewed or 
## approved by CIS and they do not guarantee that their use will result 
## in compliance with the CIS baseline. 
##
##############################################################################
## Notes
## OpenSCAP User Manual
## http://static.open-scap.org/openscap-1.0/oscap_user_manual.html
##
##############################################################################

%packages

##@security-tools
openscap
openscap-scanner
openscap-utils
scap-security-guide

xmlsec1 
xmlsec1-openssl

unzip
tar
gzip

%end

##%addon org_fedora_oscap
##        content-type = scap-security-guide
##        profile = common
##%end

## Find profiles:
## $ oscap info scap/ssg-centos7-xccdf.xml 
## Profiles: standard, pci-dss, rht-ccp, common, 
## stig-rhel7-server-upstream, ospp-rhel7-server

%post --log=/root/c7-oscap-none.log

echo "** c7-oscap-none START"

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening
OSCAPDIR=/root/oscap_results

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

if [ ! -d "${OSCAPDIR}" ];  then mkdir -p ${OSCAPDIR}; fi

####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

# Download latest evaulation proceedures

curl -o ${OSCAPDIR}/evaluate.txt  -L https://bitbucket.org/carlisle/hardening-ks/raw/master/evaluate.txt

curl -o ${OSCAPDIR}/oscap-evaluate_centos7_0.1.66.sh -L https://bitbucket.org/carlisle/hardening-ks/raw/master/centos7/oscap-evaluate_centos7_0.1.66.sh

##########################
## security audit script
##########################

cat > ${BACKUPDIR}/audit_security.sh << 'EOF'
# Security Tests

printf "========================================\n"
printf "= Performing Security Tests = $(date +%F) at $(date +%H%M) =\n"
printf "========================================\n"
printf "List all filesystems: \n\n"
df --local -P | awk {'if (NR!=1) print $6'}

printf "========================================\n"
printf "Show system executables that don't have root ownership: \n\n"
find /bin/ /usr/bin/ /usr/local/bin/ /sbin/ /usr/sbin/ /usr/local/sbin/ /usr/libexec \! -user root -exec ls -l {} \;

printf "========================================\n"
printf "Show files that differ from expected file hashes\n"
printf "These will report files modified due to hardening: \n\n"
rpm -Va | grep '^..5'

printf "========================================\n"
printf "Find SUID Executables:\n\n"
## CIS 9.1.13 Find SUID System Executables
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type f -perm -4000 -print

printf "========================================\n"
printf "Verfiy integrity of the SUID binaries returned by above:\n\n"
## CCE-80133-2 Ensure All SUID Executables Are Authorized
SUIDFILES=$(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type f -perm -4000 -print)
for I in $SUIDFILES; do echo "Integrity of $I:  "; rpm -V $(rpm -qf $I ); echo; done

printf "========================================\n"
printf "Find SGID Executables:\n\n"
# CIS 9.1.14 Find SGID System Executables
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type f -perm -2000 -print

printf "========================================\n"
printf "Verfiy integrity of SGID binaries returned by above:\n\n"
## CCE-80132-4 Ensure All SGID Executables Are Authorized 
SGIDFILES=$(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type f -perm -2000 -print)
for I in $SGIDFILES; do echo "Integrity of $I:  "; rpm -V $(rpm -qf $I ); echo; done

printf "========================================\n"
printf "Verify that All World-Writable Directories Have Sticky Bits Set:\n\n"
## CIS 1.1.21  Ensure sticky bit is set on all world-writable directories
## CCE-80130-8 Verify that All World-Writable Directories Have Sticky Bits Set
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null

printf "========================================\n"
printf "Ensure No World-Writable Files Exist:\n\n"
## CIS 9.1.10 Find World Writable Files
## CCE-80131-6 Ensure No World-Writable Files Exist
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type f -perm -0002

printf "========================================\n"
printf "Find Un-owned Files and Directories\n\n"
# Ensure All Files Are Owned by a User
# CIS 9.1.11 Find Un-owned Files and Directories
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -nouser -ls

printf "========================================\n"
printf "Find Un-grouped Files and Directories\n\n"
# Ensure All Files Are Owned by a Group
# CIS 9.1.12 Find Un-grouped Files and Directories
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -nogroup -ls

printf "========================================\n"
printf "Ensure All World-Writable Directories Are Owned by a System Account\n\n"
# Ensure All World-Writable Directories Are Owned by a System Account
# Assumes system accounts have uid < $FIRSTUSER
FIRSTUSER=1000
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type d -perm -0002 -uid +$FIRSTUSER -print

printf "========================================\n"
printf "Verify integrity of passwd, shadow, and group files\n\n"
# Verify integrity of passwd shadow and group files

pwck -r
grpck -r 

printf "========================================\n"
printf "Ensure Password Fields are Not Empty\n\n"
# CIS 9.2.1 Ensure Password Fields are Not Empty
/bin/cat /etc/shadow | /bin/awk -F: '($2 == "" ) { print $1 " does not have a password "}' 

printf "========================================\n"
printf "Check each user directory for .rhosts, .netrc, or .forward files\n\n"
# CIS 9.2.10 Check for Presence of User .rhosts Files
# CIS 9.2.18 Check for Presence of User .netrc Files
# CIS 9.2.19 Check for Presence of User .forward Files
HOMEDIR=$(/bin/cat /etc/passwd | /bin/egrep -v '(root|halt|sync|shutdown)' \
 | /bin/awk -F: '($7 != "/sbin/nologin") { print $6 }')
for file in ${HOMEDIR}/.rhosts; do
    if [ ! -h "${FILE}" -a -f "${FILE}" ]; then
      echo ".rhosts file ${HOMEDIR}/.rhosts exists"
      echo ".netrc file ${HOMEDIR}/.netrc exists"
      echo ".forward file ${HOMEDIR}/.forward exists"
    fi 
done

printf "========================================\n"
printf "sysctl configuration:\n"
printf "The following should be set to 0:\n\n"

sysctl --all | grep net.ipv4.conf.default.send_redirects
sysctl --all | grep net.ipv4.conf.all.send_redirects
sysctl --all | grep "net.ipv4.ip_forward "
sysctl --all | grep net.ipv4.conf.all.accept_source_route
sysctl --all | grep net.ipv4.conf.all.accept_redirects
sysctl --all | grep net.ipv4.conf.all.secure_redirects
sysctl --all | grep fs.suid_dumpable 

printf "\nThe following should be set to 1:\n\n" 
sysctl --all | grep net.ipv4.conf.all.log_martians
sysctl --all | grep net.ipv4.conf.default.log_martians
sysctl --all | grep net.ipv4.icmp_echo_ignore_broadcasts
sysctl --all | grep net.ipv4.icmp_ignore_bogus_error_responses

printf "\nThe following should be set to 0:\n\n"
sysctl --all | grep net.ipv4.conf.default.accept_source_route
sysctl --all | grep net.ipv4.conf.all.accept_redirects
sysctl --all | grep net.ipv4.conf.default.secure_redirects
#sysctl --all | grep net.ipv4.icmp_echo_ignore_broadcasts
#sysctl --all | grep net.ipv4.icmp_ignore_bogus_error_responses
sysctl --all | grep net.ipv4.tcp_syncookies
sysctl --all | grep net.ipv4.conf.all.rp_filter
sysctl --all | grep net.ipv4.conf.default.rp_filter
sysctl --all | grep net.ipv6.conf.all.disable_ipv6
sysctl --all | grep "net.ipv6.conf.all.accept_ra "
sysctl --all | grep "net.ipv6.conf.default.accept_ra "
sysctl --all | grep net.ipv6.conf.all.accept_redirects
sysctl --all | grep net.ipv6.conf.default.accept_redirects

printf "========================================\n"
printf "other\n\n"
 
#are these running: rxinetd telnet-server rsh-server ypserv tftp-server
#find -type f -name .rhosts -exec rm -f '{}' \;
#rm /etc/hosts.equiv

EOF

#####################
## DEPLOY NEW FILES
#####################

chown root:root ${OSCAPDIR}/oscap-evaluate_*.sh
chmod 700       ${OSCAPDIR}/oscap-evaluate_*.sh

echo "** c7-oscap-none COMPLETE"

%end
