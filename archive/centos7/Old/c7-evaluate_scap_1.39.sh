#!/usr/bin/env bash

# This evaluates against SCAP 0.1.39 downloaded from github 
# Release Notes: https://github.com/OpenSCAP/scap-security-guide/releases

# Usage: ./c7-evaluate_scap_1.39.sh >> scap_1.39.log 2>> scap_1.39.log &

# Evaluate all profiles

HOST=$(hostname)
DATE=$(date +%F)

TARGETDIR=/root/openscap_data/

OS=centos7
#OS=rhel7

# Download profile from remote site:

VERSION=0.1.39
CONTENT=${TARGETDIR}/scap-security-guide-${VERSION}

curl -o ${TARGETDIR}/scap-security-guide-${VERSION}.zip -L https://github.com/OpenSCAP/scap-security-guide/releases/download/v${VERSION}/scap-security-guide-${VERSION}.zip

#wget https://github.com/OpenSCAP/scap-security-guide/releases/download/v${VERSION}/scap-security-guide-${VERSION}.zip

unzip -o ${TARGETDIR}/scap-security-guide-${VERSION}.zip -d ${TARGETDIR}

# To extract the list of profiles:
#oscap info ${CONTENT}/ssg-${OS}-ds.xml | grep profile | sed 's+.*profile_++'

# The following array processes all available profiles, comment out the ones that are not needed

PARRAY=(

# Standard System Security Profile
	standard
# Common Profile for General-Purpose Systems
#	common
# PCI-DSS v3 Control Baseline for Red Hat Enterprise Linux 7
	pci-dss
#	pci-dss_centric
# U.S. Government Commercial Cloud Services (C2S) for Red Hat
# Enterprise Linux 7
	C2S
#	C2S-docker
# Red Hat Corporate Profile for Certified Cloud Providers (RH CCP)
	rht-ccp
# Docker Host
#        docker-host
# Security Technical Implementation Guide (STIG) for Red Hat Enterprise
# Linux 7 Server
	stig-rhel7-disa
#	stig-rhevh-upstream
#        stig-ansible-tower-upstream
#	stig-http-disa
#        stig-ipa-server-upstream
#        stig-satellite-upstream
# United States Government Configuration Baseline (USGCB / STIG)
#	ospp-rhel7
# Committee on National Security Systems Instruction (CNSSI) No. 1253, Security 
# Categorization and Control Selection for National Security Systems on security 
# controls to meet low confidentiality, low integrity, and low assurance.
	nist-800-171-cui
# Criminal Justice Information Services (CJIS) Security Policy
#	cjis-rhel7-server
# Health Insurance Portability and Accountability Act	
        hipaa
# Agence nationale de la sécurité des systèmes d'information
#	anssi_nt28_minimal
#	anssi_nt28_intermediary
#	anssi_nt28_enhanced
#	anssi_nt28_high

 	)

for PROFILE in "${PARRAY[@]}"; do 

	printf "\n#### %s ####\n\n" ${PROFILE}

	# Evaluate each profile against oval downloaded from RedHat

	oscap xccdf eval --fetch-remote-resources  --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
	 --results ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.xml \
	 --report  ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.html \
	 ${CONTENT}/ssg-${OS}-ds.xml;

	# Generate remediation script for each profile

	oscap xccdf generate fix --template urn:xccdf:fix:script:sh \
	 --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
	 --output ${TARGETDIR}/remediation-${HOST}-${DATE}-${PROFILE}.sh \
	 ${CONTENT}/ssg-${OS}-ds.xml;

        # Generate Guide for each profile 

#       oscap xccdf generate guide --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
#        --output ${TARGETDIR}/guide-${VERSION}-${PROFILE}.html \
#        ${CONTENT}/ssg-${OS}-ds.xml;

done

tar -cvzf ${HOST}-${DATE}-scap_${VERSION}.tar.gz ${HOST}-${DATE}-*.xml ${HOST}-${DATE}-*.html remediation-${HOST}-${DATE}-*.sh guide-${VERSION}-*.html

