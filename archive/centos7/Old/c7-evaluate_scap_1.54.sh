#!/usr/bin/env bash

# This evaluates against SCAP 0.1.54 provided by the scap-security-guide-0.1.54-3 rpm
# Release Notes: https://github.com/OpenSCAP/scap-security-guide/releases/tag/v0.1.36

# Usage: ./c7-evaluate_scap_1.54.sh >> scap_1.54.log 2>> scap_1.54.log &

# Evaluate all profiles

HOST=$(hostname)
DATE=$(date +%F)

TARGETDIR=/root/openscap_data/

OS=centos7
#OS=rhel7

VERSION=0.1.54

# Use content from installed package
CONTENT=/usr/share/xml/scap/ssg/content

# To extract the list of profiles:
#oscap info ${CONTENT}/ssg-${OS}-ds.xml | grep profile | sed 's+.*profile_++'

PARRAY=(
# Standard System Security Profile
	standard
# PCI-DSS v3 Control Baseline for Red Hat Enterprise Linux 7
	pci-dss
	pci-dss_centric
# National Checklist Program defined by the NIST SP 800-70
	ncp
# United States Government Configuration Baseline (USGCB / STIG)
	ospp
# Red Hat Corporate Profile for Certified Cloud Providers (RH CCP)
	rht-ccp
# Center for Internet Security Benchmark
	cis
# NIST Controlled Unclassified Information 
	cui
# Criminal Justice Information Services (CJIS) Security Policy
	cjis
# Red Hat Enterprise Linux Hypervisor Protection Profile 
# for Virtualization
	rhelh-vpp
# U.S. Government Commercial Cloud Services (C2S) for Red Hat
# Enterprise Linux 7
	C2S
# Essential 8
	e8
# Agence Nationale de la Sécurité des Systèmes d Informatio
	anssi_nt28_minimal
	anssi_nt28_intermediary
	anssi_nt28_high
# Health Insurance Portability and Accountability Act
	hipaa
# Security Technical Implementation Guide (STIG) for Red Hat Enterprise
# Linux 7 Server
	stig
	rhelh-stig
 	)

# Download oval files from redhat if it doesn't exist
#curl -O https://www.redhat.com/security/data/oval/com.redhat.rhsa-all.xml

OVALFILE=com.redhat.rhsa-RHEL7.xml

if [ ! -r ${TARGETDIR}/${OVALFILE} ]; then
   echo "File ${TARGETDIR}/${OVALFILE} does not exist. Attempting to download it"
   if curl --fail -o ${TARGETDIR}/${OVALFILE}.bz2 https://www.redhat.com/security/data/oval/${OVALFILE}.bz2; then
       bunzip2 ${TARGETDIR}/${OVALFILE}.bz2
   else
       echo "Download failed"
       exit 1
   fi
else
   echo "File ${TARGETDIR}/${OVALFILE} exists."
fi


for PROFILE in "${PARRAY[@]}"; do 

        printf "\n#### %s ####\n\n" ${PROFILE}

# using remote resources	
#	oscap xccdf eval --fetch-remote-resources  --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
#	 --results ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.xml \
#	 --report  ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.html \
#	 ${CONTENT}/ssg-${OS}-ds.xml;

	# Evaluate each profile using previously downloaded oval file
       	oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
         --results ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.xml \
         --report  ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.html \
         ${CONTENT}/ssg-${OS}-ds.xml;

	# Generate remediation script for each profile

	oscap xccdf generate fix --template urn:xccdf:fix:script:sh \
	 --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
	 --output ${TARGETDIR}/remediation-${HOST}-${DATE}-${PROFILE}.sh \
	 ${CONTENT}/ssg-${OS}-ds.xml;

	# Generate Guide for each profile 

#	oscap xccdf generate guide --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
# 	 --output ${TARGETDIR}/guide-${VERSION}-${PROFILE}.html \
# 	 ${CONTENT}/ssg-${OS}-ds.xml;

done

tar -cvzf ${HOST}-${DATE}-scap_${VERSION}.tar.gz ${HOST}-${DATE}-*.xml ${HOST}-${DATE}-*.html remediation-${HOST}-${DATE}-*.sh 

# END

