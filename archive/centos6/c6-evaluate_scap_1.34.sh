#! /usr/bin/env bash

# Evaluate all profiles, updated for Centos 6.9

HOST=$(hostname)
DATE=$(date +%F)

TARGETDIR=/root/openscap_data/
VERSION=0.1.34

OS=centos6
#OS=rhel6

CONTENT=${TARGETDIR}/scap-security-guide-${VERSION}

# If using scap security guide provided by rpm, use this directory:
#CONTENT=/usr/share/xml/scap/ssg/content

# Download security guide

curl -o scap-security-guide-${VERSION}.zip -L https://github.com/OpenSCAP/scap-security-guide/releases/download/v${VERSION}/scap-security-guide-${VERSION}.zip

#wget https://github.com/OpenSCAP/scap-security-guide/releases/download/v${VERSION}/scap-security-guide-${VERSION}.zip

unzip -o scap-security-guide-${VERSION}.zip

# To extract the list of profiles:
#oscap info ${CONTENT}/ssg-${OS}-ds.xml | grep profile | cut -d '_' -f 4

PARRAY=(

# Standard System Security Profile
    standard
# Example Server Profile
    CS2
# Common Profile for General-Purpose Systems
    common
# Server Baseline
    server
    ftp-server
# Security Technical Implementation Guide (STIG) is published as a tool
# to improve the security of Department of Defense (DoD) information systems.
# Upstream STIG for Red Hat Enterprise Linux 6 Server
    stig-rhel6-workstation-upstream
    stig-rhel6-server-gui-upstream
    stig-rhel6-server-upstream
# United States Government Configuration Baseline (USGCB)
    usgcb-rhel6-server
# Red Hat Corporate Profile for Certified Cloud Providers (RH CCP)
    rht-ccp
# Centralized Super Computing Facility (CSCF) RHEL6 Multi-Levels Security (MLS) Core Baseline
    CSCF-RHEL6-MLS
# U.S. Government Commercial Cloud Services (C2S) for Red Hat Enterprise Linux 6
    C2S
# PCI-DSS v3 Control Baseline for Red Hat Enterprise Linux 6   
    pci-dss
# Committee on National Security Systems Instruction (CNSSI) No. 1253,
# "Security Categorization and Control Selection for National Security Systems"
# on security controls to meet low confidentiality, low integrity, and low assurance."
    nist-cl-il-al
# Federal Information Security Management Act
    fisma-medium-rhel6-server
       )

for PROFILE in "${PARRAY[@]}"; do 

	printf "\n#### %s ####\n\n" ${PROFILE}

	# Evaluate each profile against oval downloaded from RedHat

	oscap xccdf eval --fetch-remote-resources  --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
	 --results ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.xml \
	 --report  ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.html \
	 ${CONTENT}/ssg-${OS}-ds.xml;

	# Generate remediation script for each profile

	oscap xccdf generate fix --template urn:xccdf:fix:script:sh \
	 --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
	 --output ${TARGETDIR}/remediation-${HOST}-${DATE}-${PROFILE}.sh \
	 ${CONTENT}/ssg-${OS}-ds.xml;

        # Generate Guide for each profile 

#       oscap xccdf generate guide --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
#        --output ${TARGETDIR}/guide-${VERSION}-${PROFILE}.html \
#        ${CONTENT}/ssg-${OS}-ds.xml;

done

tar -cvzf ${HOST}-${DATE}-scap_${VERSION}.tar.gz ${HOST}-${DATE}-*.xml ${HOST}-${DATE}-*.html remediation-${HOST}-${DATE}-*.sh guide-${VERSION}-*.html

