#! /usr/bin/env bash

# This evaluates against SCAP 0.1.28 provided by the scap-security-guide-0.1.28 rpm
# Release Notes: https://github.com/OpenSCAP/scap-security-guide/releases/tag/v0.1.28

HOST=$(hostname)
DATE=$(date +%F)

TARGETDIR=/root/openscap_data/
VERSION=0.1.28

OS=centos6
#OS=rhel6

# Use content from installed package
CONTENT=/usr/share/xml/scap/ssg/content

# To extract the list of profiles:
#oscap info ${CONTENT}/ssg-${OS}-ds.xml | grep profile | cut -d '_' -f 4

PARRAY=( 
# Standard System Security Profile
	standard
# Example Server Profile
	CS2
# Common Profile for General-Purpose Systems
	common
# Server Baseline
	server
# Upstream STIG for Red Hat Enterprise Linux 6 Server
	stig-rhel6-server-upstream
# United States Government Configuration Baseline (USGCB)
	usgcb-rhel6-server
# Red Hat Corporate Profile for Certified Cloud Providers (RH CCP)
	rht-ccp
# Centralized Super Computing Facility (CSCF) RHEL6 Multi-Levels Security (MLS) Core Baseline
	CSCF-RHEL6-MLS
# U.S. Government Commercial Cloud Services (C2S) for Red Hat Enterprise Linux 6
	C2S
# PCI-DSS v3 Control Baseline for Red Hat Enterprise Linux 6	
	pci-dss
# Committee on National Security Systems Instruction (CNSSI) No. 1253, 
# "Security Categorization and Control Selection for National Security Systems" 
# on security controls to meet low confidentiality, low integrity, and low assurance."
	nist-cl-il-al
       )

# Download oval files from redhat if it doesn't exist
#curl -O https://www.redhat.com/security/data/oval/com.redhat.rhsa-all.xml

OVALFILE=com.redhat.rhsa-RHEL6.xml

if [ ! -r ${OVALFILE} ]; then
   echo "File ${OVALFILE} does not exist. Attempting to download it"
   if curl --fail -O https://www.redhat.com/security/data/oval/${OVALFILE}.bz2; then
       bunzip2 ${OVALFILE}.bz2
   else
       echo "Download failed"
       exit 1
   fi
fi

for PROFILE in "${PARRAY[@]}"; do 

	printf "\n#### %s ####\n\n" ${PROFILE}

# using remote resources	
#	oscap xccdf eval --fetch-remote-resources  --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
#	 --results ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.xml \
#	 --report  ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.html \
#	 ${CONTENT}/ssg-${OS}-ds.xml;

	# Evaluate each profile using downloaded oval file
       	oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
         --results ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.xml \
         --report  ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.html \
         ${CONTENT}/ssg-${OS}-ds.xml ${OVALFILE} ;

	# Generate remediation script for each profile

	oscap xccdf generate fix --template urn:xccdf:fix:script:sh \
	 --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
	 --output ${TARGETDIR}/remediation-${HOST}-${DATE}-${PROFILE}.sh \
	 ${CONTENT}/ssg-${OS}-ds.xml;

	# Generate Guide for each profile 

#	oscap xccdf generate guide --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
# 	 --output ${TARGETDIR}/guide-${VERSION}-${PROFILE}.html \
# 	 ${CONTENT}/ssg-${OS}-ds.xml;

done

tar -cvzf ${HOST}-${DATE}-scap_${VERSION}.tar.gz ${HOST}-${DATE}-*.xml ${HOST}-${DATE}-*.html remediation-${HOST}-${DATE}-*.sh guide-${VERSION}-*.html

