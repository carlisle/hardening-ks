##############################################################################
## fedora-repos
## Copyright (C) 2023 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## OpenSCAP Security Guide to the Secure Configuration of Fedora
## https://static.open-scap.org/ssg-guides/ssg-fedora-guide-index.html
##
## Fedora Documentation
## https://docs.fedoraproject.org/en-US/docs/
##
## CIS = Center for Internet Security Benchmarks
## https://www.cisecurity.org/cis-benchmarks/
##
## CCE = US National Insitute of Standards and Technology Common Configuration Enumeration
## https://ncp.nist.gov/cce
##
## These scripts are inspired by various Benchmarks by The Center for 
## Internet Security. However, these scripts have not been reviewed or 
## approved by CIS and they do not guarantee that their use will result 
## in compliance with the CIS baseline. 
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this 
## guidance assume no responsibility whatsoever for its use by other parties,
## and makes no guarantees, expressed or implied, about its quality, 
## reliability, or any other characteristic.
##
##############################################################################
## Notes
##
##############################################################################


# Package Repositories
url --metalink="https://mirrors.fedoraproject.org/metalink?repo=fedora-$releasever&arch=$basearch"

repo --name=fedora          --metalink="https://mirrors.fedoraproject.org/metalink?repo=fedora-$releasever&arch=$basearch"
repo --name=fedora-modular  --metalink="https://mirrors.fedoraproject.org/metalink?repo=fedora-modular-$releasever&arch=$basearch"

repo --name=updates         --metalink="https://mirrors.fedoraproject.org/metalink?repo=updates-released-f$releasever&arch=$basearch"
repo --name=updates-modular --metalink="https://mirrors.fedoraproject.org/metalink?repo=updates-released-modular-f$releasever&arch=$basearch"

#repo --name=rpmfusion-free --metalink="https://mirrors.rpmfusion.org/metalink?repo=free-fedora-$releasever&arch=$basearch"
#repo --name=rpmfusion-free-updates --metalink="https://mirrors.rpmfusion.org/metalink?repo=free-fedora-updates-released-$releasever&arch=$basearch"

#repo --name=rpmfusion-nonfree --metalink="https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-$releasever&arch=$basearch"
#repo --name=rpmfusion-nonfree-updates --metalink="https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-updates-released-$releasever&arch=$basearch"

#repo --name=rpmfusion-free-tainted    --metalink="https://mirrors.rpmfusion.org/metalink?repo=free-fedora-tainted-$releasever&arch=$basearch"
#repo --name=rpmfusion-nonfree-tainted --metalink="https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-tainted-$releasever&arch=$basearch"


%post --log=/root/fedora-repos.log

#timestamp
echo "** fedora-repos START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

VAR='example'

BACKUPDIR=/root/KShardening/fedora-repos
 AUDITDIR=/root/KShardening/audit-scripts/
  POSTDIR=/root/KShardening/postinstall-scripts/


#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

/bin/cp -Rfpd /etc/yum.repos.d ${BACKUPDIR}/yum.repos.d-DEFAULT

####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

cat > ${BACKUPDIR}/repo-reminders.txt << 'EOF'

echo "Use Varible: ${VAR}"
#Data1
#Data2

EOF

#####################
## DEPLOY NEW FILES
#####################

#/bin/cp -f ${BACKUPDIR}/CONFIG.conf /etc/CONFIG.conf
#/bin/chown root:root /etc/CONFIG.conf
#/bin/chmod       600 /etc/CONFIG.conf


#################
## CREATE POSTINSTALL SCRIPT
#################

#if [ ! -d "${POSTDIR}" ]; then mkdir -p ${POSTDIR}; fi

#cat > ${POSTDIR}/audit_script.sh << 'EOFPOST'
#printf "** POST INSTALL STUFF" $(date +%F-%H%M-%S)
#EOFPOST


#################
## CREATE AUDIT SCRIPT
#################

#if [ ! -d "${AUDITDIR}" ]; then mkdir -p ${AUDITDIR}; fi

#cat > ${AUDITDIR}/audit_script.sh << 'EOFAUDIT'
#printf "** AUDIT STUFF" $(date +%F-%H%M-%S)
#EOFAUDIT


echo "** fedora-repos COMPLETE" $(date +%F-%H%M-%S)

%end
