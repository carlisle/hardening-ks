How to get the latest OpenSCAP profiles and run a scan with them on Centos 7:
Centos 6 will be similar but the profile names will be different

Find the latest version of this file at:
https://bitbucket.org/carlisle/hardening-ks/raw/master/evaluate.txt

References:

* http://static.open-scap.org/openscap-1.0/oscap_user_manual.html

* http://static.open-scap.org/ssg-guides/ssg-centos7-guide-index.html

* https://securityblog.redhat.com/tag/xccdf/


COPR repository that provides unofficial builds of latest versions of openscap, scap-security-guide, scap-workbench and openscap-daemon packages.

* https://copr.fedorainfracloud.org/coprs/openscapmaint/openscap-latest/


Verify that the openscap-scanner package is installed.

Download the latest OpenSCAP profile and uncompress it:

mkdir /root/openscap_data

cd /root/openscap_data

wget https://github.com/OpenSCAP/scap-security-guide/releases/download/v0.1.28/scap-security-guide-0.1.28.zip

unzip scap-security-guide-0.1.28.zip

OR

curl -o scap-security-guide-0.1.28.zip  -L https://github.com/OpenSCAP/scap-security-guide/releases/download/v0.1.28/scap-security-guide-0.1.28.zip

unzip scap-security-guide-0.1.28.zip

# Download source

wget https://fedorahosted.org/releases/o/p/openscap/openscap-1.2.8.tar.gz
wget https://fedorahosted.org/releases/o/p/openscap/openscap-1.2.8.tar.gz.sha1sum
wget https://fedorahosted.org/releases/o/p/openscap/openscap-1.2.8.tar.gz.sha512sum
sha1sum --check openscap-1.2.8.tar.gz.sha1sum
sha512sum --check openscap-1.2.8.tar.gz.sha512sum
tar -xvzf openscap-1.2.8.tar.gz


ls -Rlarth ./scap-security-guide-0.1.28

> ./scap-security-guide-0.1.28:
> total 25M
> -rw-r--r--. 1 root root  92K Jan 24 18:23 ssg-jre-ds.xml
> -rw-r--r--. 1 root root 185K Jan 24 18:23 ssg-firefox-ds.xml
> -rw-r--r--. 1 root root 1.2M Jan 24 18:23 ssg-fedora-ds.xml
> -rw-r--r--. 1 root root 431K Jan 24 18:23 ssg-debian8-ds.xml
> -rw-r--r--. 1 root root 225K Jan 24 18:23 ssg-chromium-ds.xml
> -rw-r--r--. 1 root root 4.1K Jan 24 18:23 README.md
> -rw-r--r--. 1 root root 1.6K Jan 24 18:23 LICENSE
> -rw-r--r--. 1 root root  841 Jan 24 18:23 Contributors.md
> -rw-r--r--. 1 root root 3.5M Jan 24 18:23 ssg-sl7-ds.xml
> -rw-r--r--. 1 root root 3.9M Jan 24 18:23 ssg-sl6-ds.xml
> -rw-r--r--. 1 root root 3.6M Jan 24 18:23 ssg-rhel7-ds.xml
> -rw-r--r--. 1 root root 4.1M Jan 24 18:23 ssg-rhel6-ds.xml
> -rw-r--r--. 1 root root 3.5M Jan 24 18:23 ssg-centos7-ds.xml
> -rw-r--r--. 1 root root 3.9M Jan 24 18:23 ssg-centos6-ds.xml
> drwxr-xr-x. 2 root root 4.0K Jan 24 18:23 kickstart
> drwxr-xr-x. 3 root root 4.0K Jan 24 18:23 .
> drwxr-xr-x. 4 root root 4.0K Jan 29 14:48 ..
> 
> ./scap-security-guide-0.1.28/kickstart:
> total 56K
> -rw-r--r--. 1 root root 5.4K Jan 24 18:23 ssg-rhel7-pci-dss-server-with-gui-oaa-ks.cfg
> -rw-r--r--. 1 root root 5.4K Jan 24 18:23 ssg-rhel7-ospp-ks.cfg
> -rw-r--r--. 1 root root 7.2K Jan 24 18:23 ssg-rhel6-usgcb-server-with-gui-ks.cfg
> -rw-r--r--. 1 root root  13K Jan 24 18:23 ssg-rhel6-stig-ks.cfg
> -rw-r--r--. 1 root root 7.2K Jan 24 18:23 ssg-rhel6-pci-dss-with-gui-ks.cfg
> drwxr-xr-x. 3 root root 4.0K Jan 24 18:23 ..
> drwxr-xr-x. 2 root root 4.0K Jan 24 18:23 .

Show profiles in current guide:
oscap info ./scap-security-guide-0.1.28/ssg-centos7-ds.xml

> Document type: Source Data Stream
> Imported: 2016-01-24T18:23:04
> 
> Stream: scap_org.open-scap_datastream_from_xccdf_ssg-rhel7-xccdf-1.2.xml
> Generated: (null)
> Version: 1.2
> Checklists:
> 	Ref-Id: scap_org.open-scap_cref_ssg-rhel7-xccdf-1.2.xml
> 		Profiles:
> 			xccdf_org.ssgproject.content_profile_standard
> 			xccdf_org.ssgproject.content_profile_pci-dss
> 			xccdf_org.ssgproject.content_profile_C2S
> 			xccdf_org.ssgproject.content_profile_rht-ccp
> 			xccdf_org.ssgproject.content_profile_common
> 			xccdf_org.ssgproject.content_profile_stig-rhel7-server-upstream
> 			xccdf_org.ssgproject.content_profile_ospp-rhel7-server
> 		Referenced check files:
> 			ssg-rhel7-oval.xml
> 				system: http://oval.mitre.org/XMLSchema/oval-definitions-5
> 			http://www.redhat.com/security/data/oval/Red_Hat_Enterprise_Linux_7.xml
> 				system: http://oval.mitre.org/XMLSchema/oval-definitions-5
> Checks:
> 	Ref-Id: scap_org.open-scap_cref_ssg-rhel7-oval.xml
> 	Ref-Id: scap_org.open-scap_cref_output--ssg-rhel7-cpe-oval.xml
> 	Ref-Id: scap_org.open-scap_cref_output--ssg-rhel7-oval.xml
> Dictionaries:
> 	Ref-Id: scap_org.open-scap_cref_output--ssg-rhel7-cpe-dictionary.xml

# To extract the list of just the profile names:
#oscap info ./scap-security-guide-0.1.28/ssg-centos7-ds.xml | grep profile | cut -d '_' -f 4


Run Scan to evaluate all profiles:

HOST=$(hostname)
DATE=$(date +%F)

TARGETDIR=/root/openscap_data/
VERSION=0.1.28

OS=centos7
#OS=rhel7

CONTENT=${TARGETDIR}/scap-security-guide-${VERSION}
#CONTENT=/usr/share/xml/scap/ssg/content

curl -o scap-security-guide-${VERSION}.zip -L https://github.com/OpenSCAP/scap-security-guide/releases/download/v${VERSION}/scap-security-guide-${VERSION}.zip

#wget https://github.com/OpenSCAP/scap-security-guide/releases/download/v${VERSION}/scap-security-guide-${VERSION}.zip

unzip -o scap-security-guide-${VERSION}.zip

# To extract the list of profiles:
#oscap info ${CONTENT}/ssg-${OS}-ds.xml | grep profile | cut -d '_' -f 4

# List of all profiles in guide

PARRAY=(
# Standard System Security Profile
	standard
# Common Profile for General-Purpose Systems
	common
# PCI-DSS v3 Control Baseline for Red Hat Enterprise Linux 7
	pci-dss
# U.S. Government Commercial Cloud Services (C2S) for Red Hat
# Enterprise Linux 7
	C2S
# Red Hat Corporate Profile for Certified Cloud Providers (RH CCP)
	rht-ccp
# Security Technical Implementation Guide (STIG) for Red Hat Enterprise
# Linux 7 Server
	stig-rhel7-server-upstream
# United States Government Configuration Baseline (USGCB / STIG)
	ospp-rhel7-server
       )

for PROFILE in "${PARRAY[@]}"; do 

        echo ${PROFILE}
        echo

# Evaluate server for compliance against given profile

	oscap xccdf eval --fetch-remote-resources  --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
	 --results ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.xml \
	 --report  ${TARGETDIR}/${HOST}-${DATE}-${PROFILE}.html \
	 ${CONTENT}/ssg-${OS}-ds.xml;

# Generate Remediation Script - uses result xml file from above

	oscap xccdf generate fix --template urn:xccdf:fix:script:sh \
	 --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
	 --output ${TARGETDIR}/remediation-${HOST}-${DATE}-${PROFILE}.sh \
	 ${CONTENT}/ssg-${OS}-ds.xml;

# Generate Guide for given profile - These are also found online

	oscap xccdf generate guide --profile xccdf_org.ssgproject.content_profile_${PROFILE} \
 	 --output ${TARGETDIR}/guide-${HOST}-${DATE}-${PROFILE}.html \
 	 ${CONTENT}/ssg-${OS}-ds.xml;

done

tar -cvzf ${HOST}-${DATE}-openscap.tar.gz *${HOST}-${DATE}*

