##############################################################################
## el9-firewalld
## Copyright (C) 2024 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/firewalld/firewalld.conf
## /etc/firewalld/zones/*
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## These scripts are inspired by various Benchmarks by The Center for Internet
## Security. However, these scripts have not been reviewed or approved by CIS
## and they do not guarantee that their use will result in compliance with the
## CIS baseline. 
##
## This benchmark is a direct port of a SCAP Security Guide benchmark developed
## for Red Hat Enterprise Linux. It has been modified through an automated
## process to remove specific dependencies on Red Hat Enterprise Linux and to
## function with any derivative of RHEL9. The result is a generally useful SCAP
## Security Guide benchmark.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this guidance
## assume no responsibility whatsoever for its use by other parties, and makes
## no guarantees, expressed or implied, about its quality, reliability, or any
## other characteristic.
##
## This script should work on any derivative of RHEL9 such as Rocky Linux 9, etc.
##
##############################################################################
## Notes
##
##############################################################################

%packages

firewalld
nftables

%end

%post --log=/root/el9-firewalld.log

#timestamp
echo "** el9-firewalld START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/firewalld/

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi
/bin/cp -Rfpd /etc/firewalld/ ${BACKUPDIR}/firewalld-DEFAULT/

####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

cat > ${BACKUPDIR}/postinstall-firewalld-changes.txt << 'EOF'

# Research: how to drop and restore firewalld configuration

# Determine primary network interface:
ip a

# set interface using ens3 as an example
IFNET="ens3"

# set default zone for all interfaces to drop
firewall-cmd --set-default-zone=drop
# allows ssh
firewall-cmd --zone=public --change-interface=${IFNET}
firewall-cmd --runtime-to-permanent
firewall-cmd --reload
#systemctl reload firewalld

# change /etc/firewalld/firewalld.conf, DefaultZone=drop


# CIS 3.4.2.4 Configure Firewalld to Trust Loopback Traffic
firewall-cmd --permanent --zone=trusted --add-interface=lo
firewall-cmd --reload


# CIS 3.4.2.4 Configure Firewalld to Restrict Loopback Traffic
firewall-cmd --permanent --zone=trusted --add-rich-rule='rule family=ipv4 source address="127.0.0.1" destination not address="127.0.0.1" drop'
firewall-cmd --permanent --zone=trusted --add-rich-rule='rule family=ipv6 source address="::1" destination not address="::1" drop'
firewall-cmd --reload


# verify settings
firewall-cmd --get-default-zone
firewall-cmd --list-all-zones

EOF

cat > ${BACKUPDIR}/postinstall-firewalld-hints.txt << 'EOF'

# How to setup Network Address Translation / IP Masquerading
#
# todo

# set recent changes to permanent
firewall-cmd --runtime-to-permanent

# Creating custom zone
firewall-cmd --permanent --new-zone=justssh
firewall-cmd --reload
firewall-cmd --zone=justssh --add-service=ssh
firewall-cmd --zone=justssh --change-interface=ens3
firewall-cmd --runtime-to-permanent
firewall-cmd --reload
firewall-cmd --get-active-zones
firewall-cmd --zone=justssh --list-services

# 2023-09-02 04:39:23 WARNING: AllowZoneDrifting is enabled. This is considered an insecure configuration option.
# It will be removed in a future release. Please consider disabling it now.

# creating a custom service
# https://firewalld.org/documentation/howto/add-a-service.html

# https://serverfault.com/questions/684602/how-to-open-port-for-a-specific-ip-address-with-firewall-cmd-on-centos

# log all dropped packets
# https://www.cyberciti.biz/faq/enable-firewalld-logging-for-denied-packets-on-linux/

firewall-cmd --get-log-denied
firewall-cmd --set-log-denied=all
firewall-cmd --get-log-denied

touch /var/log/firewalld-dropped.log

edit /etc/rsyslog.d/firewalld-dropped.conf
:msg,contains,"_DROP" /var/log/firewalld-dropped.log
:msg,contains,"_REJECT" /var/log/firewalld-dropped.log
& stop

edit /etc/logrotate.d/firewalld-dropped
/var/log/firewalld-dropped.log {

}


EOF

####################
## TURN ON SERVICE
####################

systemctl enable  firewalld
systemctl disable nftables
systemctl mask    nftables

#timestamp
echo "** el9-firewalld COMPLETE" $(date +%F-%H%M-%S)

%end
