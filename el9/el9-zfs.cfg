##############################################################################
## el9-zfs
## Copyright (C) 2023 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
## 
## https://zfsonlinux.org/
## https://openzfs.github.io/openzfs-docs/Getting%20Started/index.html
## https://openzfs.github.io/openzfs-docs/Getting%20Started/RHEL-based%20distro/index.html#dkms
##
## https://wiki.archlinux.org/title/ZFS
##
## These scripts are inspired by various Benchmarks by The Center for Internet
## Security. However, these scripts have not been reviewed or approved by CIS
## and they do not guarantee that their use will result in compliance with the
## CIS baseline. 
##
## This benchmark is a direct port of a SCAP Security Guide benchmark developed
## for Red Hat Enterprise Linux. It has been modified through an automated
## process to remove specific dependencies on Red Hat Enterprise Linux and to
## function with any derivative of RHEL9. The result is a generally useful SCAP
## Security Guide benchmark.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this guidance
## assume no responsibility whatsoever for its use by other parties, and makes
## no guarantees, expressed or implied, about its quality, reliability, or any
## other characteristic.
##
## This script should work on any derivative of RHEL9 such as Rocky Linux 9, etc.
##
##############################################################################
## Notes
## Considerations for deploying ZFS on Linux and mounting storage using NFS
##
##############################################################################

%packages

lsof
iotop

nfs-utils 

# samba ?

kernel-devel
kernel-devel-matched

zip

gcc-c++
gcc-plugin-annobin
annobin
libstdc++-devel
systemtap-sdt-devel

avahi-libs
libdatrie
libthai
libtirpc
libuv
llvm-libs
lm_sensors-libs
pcp-conf
pcp-libs

dkms
dwz
sombok
sysstat

efi-srpm-macros
fonts-srpm-macros
ghc-srpm-macros
go-srpm-macros
kernel-srpm-macros
lua-srpm-macros
ocaml-srpm-macros
openblas-srpm-macros
pyproject-srpm-macros
python-srpm-macros
python3-pyparsing
qt5-srpm-macros
redhat-rpm-config
rust-srpm-macros

perl
perl-Algorithm-Diff
perl-Archive-Tar
perl-Archive-Zip
perl-Attribute-Handlers
perl-AutoSplit
perl-Benchmark
perl-CPAN
perl-CPAN-Meta
perl-CPAN-Meta-Requirements
perl-CPAN-Meta-YAML
perl-Compress-Bzip2
perl-Compress-Raw-Bzip2
perl-Compress-Raw-Lzma
perl-Compress-Raw-Zlib
perl-Config-Extensions
perl-Config-Perl-V
perl-DBM_Filter
perl-DB_File
perl-Data-OptList
perl-Data-Section
perl-Devel-PPPort
perl-Devel-Peek
perl-Devel-SelfStubber
perl-Devel-Size
perl-Digest-SHA
perl-Digest-SHA1
perl-DirHandle
perl-Dumpvalue
perl-DynaLoader
perl-Encode-devel
perl-English
perl-Env
perl-ExtUtils-CBuilder
perl-ExtUtils-Command
perl-ExtUtils-Constant
perl-ExtUtils-Embed
perl-ExtUtils-Install
perl-ExtUtils-MM-Utils
perl-ExtUtils-MakeMaker
perl-ExtUtils-Manifest
perl-ExtUtils-Miniperl
perl-ExtUtils-ParseXS
perl-File-Compare
perl-File-Copy
perl-File-DosGlob
perl-File-Fetch
perl-File-Find
perl-File-HomeDir
perl-File-Which
perl-FileCache
perl-Filter
perl-Filter-Simple
perl-FindBin
perl-GDBM_File
perl-Hash-Util
perl-Hash-Util-FieldHash
perl-I18N-Collate
perl-I18N-LangTags
perl-I18N-Langinfo
perl-IO-Compress
perl-IO-Compress-Lzma
perl-IO-Zlib
perl-IPC-Cmd
perl-IPC-SysV
perl-IPC-System-Simple
perl-Importer
perl-JSON-PP
perl-Locale-Maketext
perl-Locale-Maketext-Simple
perl-MIME-Charset
perl-MRO-Compat
perl-Math-BigInt
perl-Math-BigInt-FastCalc
perl-Math-BigRat
perl-Math-Complex
perl-Memoize
perl-Module-Build
perl-Module-CoreList
perl-Module-CoreList-tools
perl-Module-Load
perl-Module-Load-Conditional
perl-Module-Loaded
perl-Module-Metadata
perl-Module-Signature
perl-NEXT
perl-Net
perl-Net-Ping
perl-ODBM_File
perl-Object-HashBase
perl-Opcode
perl-Package-Generator
perl-Params-Check
perl-Params-Util
perl-Perl-OSType
perl-PerlIO-via-QuotedPrint
perl-Pod-Checker
perl-Pod-Functions
perl-Pod-Html
perl-Safe
perl-Search-Dict
perl-SelfLoader
perl-Software-License
perl-Sub-Exporter
perl-Sub-Install
perl-Sys-Hostname
perl-Sys-Syslog
perl-Term-Complete
perl-Term-ReadLine
perl-Term-Size-Perl
perl-Term-Table
perl-Test
perl-Test-Harness
perl-Test-Simple
perl-Text-Abbrev
perl-Text-Balanced
perl-Text-Diff
perl-Text-Glob
perl-Text-Template
perl-Thread
perl-Thread-Queue
perl-Thread-Semaphore
perl-Tie
perl-Tie-File
perl-Tie-Memoize
perl-Tie-RefHash
perl-Time
perl-Time-HiRes
perl-Time-Piece
perl-Unicode-Collate
perl-Unicode-Normalize
perl-Unicode-UCD
perl-User-pwent
perl-autodie
perl-autouse
perl-bignum
perl-blib
perl-debugger
perl-deprecate
perl-devel
perl-diagnostics
perl-doc
perl-encoding
perl-encoding-warnings
perl-experimental
perl-fields
perl-filetest
perl-inc-latest
perl-less
perl-lib
perl-libnetcfg
perl-local-lib
perl-locale
perl-macros
perl-meta-notation
perl-open
perl-perlfaq
perl-ph
perl-sigtrap
perl-sort
perl-srpm-macros
perl-threads
perl-threads-shared
perl-utils
perl-version
perl-vmsish
perl-CPAN-DistnameInfo
perl-Encode-Locale
perl-Term-Size-Any
perl-TermReadKey
perl-Unicode-LineBreak

%end

%post --log=/root/el9-zfs.log

#timestamp
echo "** el9-zfs START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/zfs/

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

#/bin/cp -fpd /etc/CONFIG.conf ${BACKUPDIR}/CONFIG.conf-DEFAULT

####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

cat > ${BACKUPDIR}/postinstall-zfs.txt << 'EOFZFS'

# Deployment proceedure

# verify the /var and in partitcular /var/lib/dkms does not have noexec set

cat /etc/mtab | grep var | grep exec


dnf install https://zfsonlinux.org/epel/zfs-release-2-3$(rpm --eval "%{dist}").noarch.rpm
If local_gpg is enabled under /etc/dnf/dnf.conf, then use
dnf --nogpg install https://zfsonlinux.org/epel/zfs-release-2-3$(rpm --eval "%{dist}").noarch.rpm

# install key
rpm --import  /etc/pki/rpm-gpg/RPM-GPG-KEY-openzfs-el-$(rpm --eval "%{dist}") 

# use DKMS install
dnf install zfs

# Handling this error:
# See https://github.com/openzfs/zfs/issues/14447
# Removing old zfs-2.1.12 DKMS files...
# Deleting module zfs-2.1.12 completely from the DKMS tree.
# Loading new zfs-2.1.12 DKMS files...
# Building for 5.14.0-284.25.1.el9_2.x86_64
# Building initial module for 5.14.0-284.25.1.el9_2.x86_64
# Warning: The pre_build script is not executable.
# Error! Bad return status for module build on kernel: 5.14.0-284.25.1.el9_2.x86_64 (x86_64)
# Consult /var/lib/dkms/zfs/2.1.12/build/make.log for more information.
# warning: %post(zfs-dkms-2.1.12-1.el9.noarch) scriptlet failed, exit status 10

# Solution: One or more directories that are required for zfs module compiling
#  has been set to noexec. Temporarity reset them to exec with:

mount -o remount,exec <dir>

Most likely directories: /var /dev/shm /tmp /var/tmp

# first time:
/sbin/modprobe zfs 

# create pool
zpool create  san1 raidz1  sda  sdb sdc

# check status
zpool status -v

# scrub
zpool scrub san1

# create dataset
zfs create san1/nfs 

# install nfs in order to do an nfs sharing from a zfs dataset

# enable nfs sharing on a dataset
zfs set sharenfs=on san1/nfs 

# show properties of dataset
zfs get all san1/nfs
zfs get sharenfs san1/nfs

# modify firewall to allow nfs
firewall-cmd --zone=private --add-service=nfs
firewall-cmd --zone=private --add-service=rpc-bind
firewall-cmd --zone=private --add-service=mountd
firewall-cmd --runtime-to-permanent
firewall-cmd --reload

firewall-cmd --get-active-zones
firewall-cmd --zone=private --list-services 

# test nfs server from client
showmount -e <target>
rpcinfo <target>
nfsstat <target>
mount target:/mount /mount 

####

# upgrading
# export pool
zool export <pool>

# show importable pools
zool import

# import poot
zpool import <pool>

# dkms zfs troubleshooting

dkms status

dkms build zfs/2.1.12
dkms install zfs/2.1.12

EOFZFS


####################
## TURN ON SERVICE
####################

systemctl enable nfs-server.service

#timestamp
echo "** el9-zfs COMPLETE" $(date +%F-%H%M-%S)

%end
