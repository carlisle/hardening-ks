##############################################################################
## el9-coredump
## Copyright (C) 2024 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/systemd/coredump.conf
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## These scripts are inspired by various Benchmarks by The Center for Internet
## Security. However, these scripts have not been reviewed or approved by CIS
## and they do not guarantee that their use will result in compliance with the
## CIS baseline. 
##
## This benchmark is a direct port of a SCAP Security Guide benchmark developed
## for Red Hat Enterprise Linux. It has been modified through an automated
## process to remove specific dependencies on Red Hat Enterprise Linux and to
## function with any derivative of RHEL9. The result is a generally useful SCAP
## Security Guide benchmark.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this guidance
## assume no responsibility whatsoever for its use by other parties, and makes
## no guarantees, expressed or implied, about its quality, reliability, or any
## other characteristic.
##
## This script should work on any derivative of RHEL9 such as Rocky Linux 9, etc.
##
##############################################################################
## Notes
##
##############################################################################

%packages

systemd
pam

%end

%post --log=/root/el9-coredump.log

#timestamp
echo "** el9-coredump START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/coredump/

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

/bin/cp -fpd /etc/systemd/coredump.conf ${BACKUPDIR}/systemd-coredump.conf-DEFAULT

####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

/bin/cp -fpd /etc/systemd/coredump.conf ${BACKUPDIR}/systemd-coredump.conf

OLDDATA="^ProcessSizeMax=1G"
NEWDATA="\n# Disable core dump backtraces\n# CIS 1.5.3\nProcessSizeMax=0\n"
LC_ALL=C sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/systemd-coredump.conf

OLDDATA="^#Storage=external"
NEWDATA="\n# Disable storing core dump\n# CIS 1.5.4\nStorage=none\n"
LC_ALL=C sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/systemd-coredump.conf


cat > ${BACKUPDIR}/sysctl-coredump.conf << 'EOF'
# Disable storing core dumps
# CUI
kernel.core_pattern =

# Configure file name of core dumps
# CUI
kernel.core_uses_pid = 0

EOF


cat > ${BACKUPDIR}/limits-coredump.conf << 'EOF'

# Disable Core Dumps for All Users
# CIS 1.6.1, CUI
*	hard	core	0

EOF

#####################
## DEPLOY NEW FILES
#####################

/bin/cp -f ${BACKUPDIR}/systemd-coredump.conf /etc/systemd/coredump.conf
/bin/chown root:root                          /etc/systemd/coredump.conf
/bin/chmod       644                          /etc/systemd/coredump.conf

/bin/cp -f ${BACKUPDIR}/sysctl-coredump.conf /etc/sysctl.d/coredump.conf
/bin/chown root:root                         /etc/sysctl.d/coredump.conf
/bin/chmod       644                         /etc/sysctl.d/coredump.conf

/bin/cp -f ${BACKUPDIR}/limits-coredump.conf /etc/security/limits.d/coredump.conf
/bin/chown root:root                         /etc/security/limits.d/coredump.conf
/bin/chmod       644                         /etc/security/limits.d/coredump.conf


####################
## TURN ON SERVICE
####################

#systemctl disable --now systemd-coredump.socket
systemctl mask --now     systemd-coredump.socket

#timestamp
echo "** el9-coredump COMPLETE" $(date +%F-%H%M-%S)

%end
