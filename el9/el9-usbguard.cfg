##############################################################################
## el9-usbguard
## Copyright (C) 2023 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/usbguard/usbguard-daemon.conf
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## These scripts are inspired by various Benchmarks by The Center for Internet
## Security. However, these scripts have not been reviewed or approved by CIS
## and they do not guarantee that their use will result in compliance with the
## CIS baseline. 
##
## This benchmark is a direct port of a SCAP Security Guide benchmark developed
## for Red Hat Enterprise Linux. It has been modified through an automated
## process to remove specific dependencies on Red Hat Enterprise Linux and to
## function with any derivative of RHEL9. The result is a generally useful SCAP
## Security Guide benchmark.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this guidance
## assume no responsibility whatsoever for its use by other parties, and makes
## no guarantees, expressed or implied, about its quality, reliability, or any
## other characteristic.
##
## This script should work on any derivative of RHEL9 such as Rocky Linux 9, etc.
##
##############################################################################
## Notes
## Allows creating of allow/deny lists of usb devices
##
##############################################################################

%packages

usbguard

%end

%post --log=/root/el9-usbguard.log

#timestamp
echo "** el9-usbguard START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/usbguard/

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi


####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

cat > ${BACKUPDIR}/postinstall-usbguard.txt << 'EOF'

# configure USBGuard daemon to log via Linux Audit (as opposed directly to a 
# file), AuditBackend option in /etc/usbguard/usbguard-daemon.conf needs to
# be set to LinuxAudit.
# "AuditBackend=LinuxAudit" >> "/etc/usbguard/usbguard-daemon.conf"

# Authorize Human Interface Devices and USB hubs in USBGuard daemon
# add the line allow with-interface match-all { 03:*:* 09:00:* } to 
# /etc/usbguard/rules.conf.

EOF

#####################
## DEPLOY NEW FILES
#####################

#/bin/cp -f ${BACKUPDIR}/CONFIG.conf /etc/CONFIG.conf
#/bin/chown root:root /etc/CONFIG.conf
#/bin/chmod       600 /etc/CONFIG.conf

####################
## TURN ON SERVICE
####################

# systemctl disable SERVICE
# systemctl enable  SERVICE

#timestamp
echo "** el9-usbguard COMPLETE" $(date +%F-%H%M-%S)

%end
