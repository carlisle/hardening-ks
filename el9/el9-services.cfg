##############################################################################
## el9-services
## Copyright (C) 2023 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## These scripts are inspired by various Benchmarks by The Center for Internet
## Security. However, these scripts have not been reviewed or approved by CIS
## and they do not guarantee that their use will result in compliance with the
## CIS baseline. 
##
## This benchmark is a direct port of a SCAP Security Guide benchmark developed
## for Red Hat Enterprise Linux. It has been modified through an automated
## process to remove specific dependencies on Red Hat Enterprise Linux and to
## function with any derivative of RHEL9. The result is a generally useful SCAP
## Security Guide benchmark.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this guidance
## assume no responsibility whatsoever for its use by other parties, and makes
## no guarantees, expressed or implied, about its quality, reliability, or any
## other characteristic.
##
## This script should work on any derivative of RHEL9 such as Rocky Linux 9, etc.
##
##############################################################################
## Notes
##
##############################################################################

%packages

### Install these services

# Ensure gnutls-utils is installed
# CUI STIG SRG-OS-000480-GPOS-00227
gnutls-utils

# Install policycoreutils-python-utils package
# Install policycoreutils Package
# CUI
policycoreutils-python-utils
policycoreutils

# Install rng-tools Package
# STIG
rng-tools

libreswan

## Hardware Tokens for Authentication
## STIG
# Install the opensc Package For Multifactor Authentication
opensc
# Install Smart Card Packages For Multifactor Authentication
openssl-pkcs11


#### Unintstall these services

# Uninstall iprutils Package
# CUI
-iprutils

# Uninstall abrt-addon-ccpp
# CUI
-abrt-addon-ccpp
-abrt-addon-kerneloops
-abrt-cli
-abrt-plugin-sosreport
-python3-abrt-addon

# needed by virtualization platform
#-gssproxy

-krb5-workstation
-libreport-plugin-logger
-libreport-plugin-rhtsupport

# Uninstall tuned Package
# STIG
-tuned

%end

%post --log=/root/el9-services.log

#timestamp
echo "** el9-services START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/services/

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

cat > ${BACKUPDIR}/postinstall-audit-services.txt << 'EOF'

Discover unnecessary and failed services which can be disabled.

# References
# https://www.tecmint.com/list-all-running-services-under-systemd-in-linux/
# https://linux-audit.com/systemd/auditing-systemd-solving-failed-units-with-systemctl/

# show all loaded systemd units
systemctl 

# show all services, note any failed services
systemctl --type=service

# show all active services
systemctl --type=service --state=active

# show all running services
systemctl --type=service --state=running

# show port a given services is running on
ss -ltup | grep <service name>

# list running firewall services
firewall-cmd --list-services

# troubleshooting failed services
systemctl | grep failed
systemctl status <failed service>

# disabling services
systemctl disable --now <service>

# masking service to insure it can never be started
systemctl mask <service>


# Review other services, clients, and configuration
dnf list dnsmasq httpd nginx squid cyrus-imapd dovecot openldap-clients xinetd *ftp*
dnf list dhcp-server rpcbind nfs-server ypserv telnet* tftp* rsync-daemon cups* samba net-snmp

# systemctl mask --now avahi-daemon.service
# sudo dnf erase dhcp-server
# sudo dnf erase dnsmasq
# sudo dnf erase ftp
# sudo dnf erase httpd
# sudo dnf erase nginx
# sudo dnf erase cyrus-imapd
# sudo dnf erase dovecot
# sudo dnf erase openldap-clients
# The software for all Mail Transfer Agents is complex and most have a long
# history of security issues. While it is important to ensure that the system
# can process local mail messages, it is not necessary to have the MTA's daemon
# listening on a port unless the server is intended to be a mail server that
# receives and processes mail from other systems.
# sudo systemctl mask --now rpcbind.service
# sudo systemctl mask --now nfs-server.service
# sudo dnf erase xinetd
# sudo dnf erase ypserv
# sudo rm /etc/hosts.equiv
# rm ~/.rhosts
# sudo dnf erase telnet-server
# dnf erase telnet
# sudo dnf erase tftp-server
# dnf erase tftp
# sudo dnf erase rsync-daemon
# sudo systemctl mask --now cups.service
# sudo dnf erase squid
# sudo dnf erase samba
# sudo dnf erase net-snmp
# sudo dnf groupremove base-x
# sudo dnf remove xorg-x11-server-common
# systemctl set-default multi-user.target

EOF

cat > ${BACKUPDIR}/postinstall-rng-test.txt << 'RNGEOF'

cat /dev/random | rngtest -c 1000

RNGEOF

####################
## TURN ON OR OFF SERVICES
####################

# Disable debug-shell SystemD Service
# CUI, NIST 
systemctl mask --now debug-shell.service

# Disable KDump Kernel Crash Analyzer (kdump)
# STIG
systemctl mask --now kdump.service

# Disable Bluetooth Service
# CIS 3.1.3
systemctl mask --now bluetooth.service

# Disable the Automounter
# The autofs daemon mounts and unmounts filesystems, such as user home
# directories shared via NFS, on demand.
# CIS 2.1.1
systemctl mask --now autofs.service

# Enable Random Number Generator Service
# https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/6/html/security_guide/sect-security_guide-encryption-using_the_random_number_generator#sect-Security_Guide-Encryption-Using_the_Random_Number_Generator
systemctl enable rngd.service

#timestamp
echo "** el9-services COMPLETE" $(date +%F-%H%M-%S)

%end
