##############################################################################
## el9-cron
## Copyright (C) 2023 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/CONFIG.conf
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## These scripts are inspired by various Benchmarks by The Center for Internet
## Security. However, these scripts have not been reviewed or approved by CIS
## and they do not guarantee that their use will result in compliance with the
## CIS baseline. 
##
## This benchmark is a direct port of a SCAP Security Guide benchmark developed
## for Red Hat Enterprise Linux. It has been modified through an automated
## process to remove specific dependencies on Red Hat Enterprise Linux and to
## function with any derivative of RHEL9. The result is a generally useful SCAP
## Security Guide benchmark.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this guidance
## assume no responsibility whatsoever for its use by other parties, and makes
## no guarantees, expressed or implied, about its quality, reliability, or any
## other characteristic.
##
## This script should work on any derivative of RHEL9 such as Rocky Linux 9, etc.
##
##############################################################################
## Notes
##
##############################################################################

%packages

cronie
crontabs

%end

%post --log=/root/el9-cron.log

#timestamp
echo "** el9-cron START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/cron/

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

/bin/cp -fpd /etc/crontab    ${BACKUPDIR}/crontab-DEFAULT
/bin/cp -fpd /etc/cron.deny  ${BACKUPDIR}/cron.deny-DEFAULT

/bin/cp -Rfpd /etc/cron.d      ${BACKUPDIR}/cron.d-DEFAULT
/bin/cp -Rfpd /etc/cron.hourly ${BACKUPDIR}/cron.hourly-DEFAULT
/bin/cp -Rfpd /etc/cron.daily  ${BACKUPDIR}/cron.daily-DEFAULT
/bin/cp -Rfpd /etc/cron.weekly ${BACKUPDIR}/cron.weekly-DEFAULT
/bin/cp -Rfpd /etc/cron.monthly ${BACKUPDIR}/cron.monthly-DEFAULT

################################################
## DEPLOY NEW FILES
## No new files, just removing or changing
## permissions on old files so that only root 
## and selected users can install cron events.
################################################

### CIS 6.1.4 Set User/Group Owner and Permission on /etc/crontab

chown root:root /etc/crontab
chmod 600 /etc/crontab

### CIS 6.1.5 Set User/Group Owner and Permission on /etc/cron.hourly

chown root:root /etc/cron.hourly
chmod 700 /etc/cron.hourly

### CIS 6.1.6 Set User/Group Owner and Permission on /etc/cron.daily

chown root:root /etc/cron.daily
chmod 700 /etc/cron.daily

### CIS 6.1.7 Set User/Group Owner and Permission on /etc/cron.weekly

chown root:root /etc/cron.weekly
chmod 700 /etc/cron.weekly

### CIS 6.1.8 Set User/Group Owner and Permission on /etc/cron.monthly

chown root:root /etc/cron.monthly
chmod 700 /etc/cron.monthly

### CIS 6.1.9 Set User/Group Owner and Permission on /etc/cron.d

chown root:root /etc/cron.d
chmod 700 /etc/cron.d

### CIS 6.1.10 Restrict at Daemon
### CIS 6.1.11 Restrict at/cron to Authorized Users
### root can always create cron and at jobs for any user
### Only those in cron.allow and at.allow create new cron
### and at jobs for themselves.

/bin/rm -f      /etc/cron.deny
touch           /etc/cron.allow
chown root:root /etc/cron.allow
chmod 0600      /etc/cron.allow

/bin/rm -f      /etc/at.deny
touch           /etc/at.allow
chown root:root /etc/at.allow
chmod 0600      /etc/at.allow


####################
## TURN ON SERVICE
####################

### CIS 6.1.2 Enable crond Daemon
systemctl enable crond.service

systemctl disable atd.service

#timestamp
echo "** el9-cron COMPLETE" $(date +%F-%H%M-%S)

%end
