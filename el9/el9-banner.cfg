##############################################################################
## el9-banner
## Copyright (C) 2024 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /etc/issue
## /etc/issue.net
## /etc/motd
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## These scripts are inspired by various Benchmarks by The Center for Internet
## Security. However, these scripts have not been reviewed or approved by CIS
## and they do not guarantee that their use will result in compliance with the
## CIS baseline. 
##
## This benchmark is a direct port of a SCAP Security Guide benchmark developed
## for Red Hat Enterprise Linux. It has been modified through an automated
## process to remove specific dependencies on Red Hat Enterprise Linux and to
## function with any derivative of RHEL9. The result is a generally useful SCAP
## Security Guide benchmark.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this guidance
## assume no responsibility whatsoever for its use by other parties, and makes
## no guarantees, expressed or implied, about its quality, reliability, or any
## other characteristic.

## This script should work on any derivative of RHEL9 such as Rocky Linux 9, etc.
##
##############################################################################
## Notes
## Gnome has its own method of setting the banner for graphical login
##
##############################################################################

%packages

setup

%end

%post --log=/root/el9-banner.log

#timestamp
echo "** el9-banner START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/banner/

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi

/bin/cp -fpd /etc/issue           ${BACKUPDIR}/issue-DEFAULT
/bin/cp -fpd /etc/issue.net       ${BACKUPDIR}/issue.net-DEFAULT
/bin/cp -fpd /etc/motd            ${BACKUPDIR}/motd-DEFAULT

####################
## WRITE NEW BANNER MESSAGES 
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

# Some sample banners

cat >> ${BACKUPDIR}/Banner.txt << 'EOFBANNER'
  Authorized uses only. All activity may be monitored and reported.
EOFBANNER

##cat >> ${BACKUPDIR}/Banner.txt << 'EOFBANNER'
#  WARNING: This service is restricted to authorized users only.
#  All activities on this system are monitored and logged.
#  Disconnect immediately if you are not an authorized user!
##EOFBANNER

#cat >> ${BACKUPDIR}/Banner.txt << 'EOFBANNER'
#You are accessing a U.S. Government (USG) Information System (IS) that is provided for USG-authorized use only. By using this IS (which includes any device attached to this IS), you consent to the following conditions:
#-The USG routinely intercepts and monitors communications on this IS for purposes including, but not limited to, penetration testing, COMSEC monitoring, network operations and defense, personnel misconduct (PM), law enforcement (LE), and counterintelligence (CI) investigations.
#-At any time, the USG may inspect and seize data stored on this IS.
#-Communications using, or data stored on, this IS are not private, are subject to routine monitoring, interception, and search, and may be disclosed or used for any USG-authorized purpose.
#-This IS includes security measures (e.g., authentication and access controls) to protect USG interests -- not for your personal benefit or privacy.
#-Notwithstanding the above, using this IS does not constitute consent to PM, LE or CI investigative searching or monitoring of the content of privileged communications, or work product, related to personal representation or services by attorneys, psychotherapists, or clergy, and their assistants. Such communications and work product are private and confidential. See User Agreement for details.
#
#EOFBANNER

#cat >> ${BACKUPDIR}/Banner.txt << 'EOFNET'
#I've read & consent to terms in IS user agreem't.
#EOFNET


/bin/cp -fpd ${BACKUPDIR}/Banner.txt ${BACKUPDIR}/issue
/bin/cp -fpd ${BACKUPDIR}/Banner.txt ${BACKUPDIR}/issue.net
/bin/cp -fpd ${BACKUPDIR}/Banner.txt ${BACKUPDIR}/motd

#####################
## DEPLOY NEW FILES
#####################

/bin/cp -fpd ${BACKUPDIR}/issue /etc/issue
/bin/chown root:root /etc/issue
/bin/chmod 0644      /etc/issue

/bin/cp -fpd ${BACKUPDIR}/issue.net /etc/issue.net
/bin/chown root:root /etc/issue.net
/bin/chmod 0644      /etc/issue.net

/bin/cp -fpd ${BACKUPDIR}/motd /etc/motd
/bin/chown root:root /etc/motd
/bin/chmod 0644      /etc/motd


#timestamp
echo "** el9-banner COMPLETE" $(date +%F-%H%M-%S)

%end
