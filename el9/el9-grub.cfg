##############################################################################
## el9-grub
## Copyright (C) 2024 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /boot/efi/
## /boot/grub2/
## /etc/grub.d/
## /etc/default/grub
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
## These scripts are inspired by various Benchmarks by The Center for Internet
## Security. However, these scripts have not been reviewed or approved by CIS
## and they do not guarantee that their use will result in compliance with the
## CIS baseline. 
##
## This benchmark is a direct port of a SCAP Security Guide benchmark developed
## for Red Hat Enterprise Linux. It has been modified through an automated
## process to remove specific dependencies on Red Hat Enterprise Linux and to
## function with any derivative of RHEL9. The result is a generally useful SCAP
## Security Guide benchmark.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this guidance
## assume no responsibility whatsoever for its use by other parties, and makes
## no guarantees, expressed or implied, about its quality, reliability, or any
## other characteristic.
##
## This script should work on any derivative of RHEL9 such as Rocky Linux 9, etc.
##
##############################################################################
## Notes
##
##############################################################################

%packages

grub2-tools
grub2-tools-minimal
grubby

%end

%post --log=/root/el9-grub.log

#timestamp
echo "** el9-grub START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/grub/

#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi
mkdir -p ${BACKUPDIR}/boot-DEFAULT/efi/
mkdir -p ${BACKUPDIR}/boot-DEFAULT/grub2/
mkdir -p ${BACKUPDIR}/grub.d-DEFAULT/
mkdir -p ${BACKUPDIR}/default-DEFAULT/

/bin/cp -Rfpd /boot/efi/*           ${BACKUPDIR}/boot-DEFAULT/efi/
/bin/cp -Rfpd /boot/grub2/*         ${BACKUPDIR}/boot-DEFAULT/grub2/
/bin/cp -Rfpd /etc/grub.d/*         ${BACKUPDIR}/grub.d-DEFAULT/
/bin/cp -Rfpd /etc/default/grub     ${BACKUPDIR}/default-DEFAULT/grub


####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

cat > ${BACKUPDIR}/postinstall-grub-set-password.txt << 'EOFPASSWD'

# https://docs.fedoraproject.org/en-US/quick-docs/bootloading-with-grub2/
# https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/ch-working_with_the_grub_2_boot_loader
#

# Set bootloader admin username. root is default
# Do not use common names like root, admin, or administrator
BACKUPDIR=/root/KShardening/grub/
BOOTADMIN="boss"

# Set the Boot Loader Admin Username to a Non-Default Value
# STIG
/bin/cp -fpd /etc/grub.d/01_users ${BACKUPDIR}/01_users-DEFAULT
/bin/cp -fpd ${BACKUPDIR}/01_users-DEFAULT ${BACKUPDIR}/01_users
sed -i "s+\(set superusers=\).*+\1\"${BOOTADMIN}\"+g" ${BACKUPDIR}/01_users

###sed -i "s+\password_pbkdf2 root+\1\"password_pbkdf2 ${BOOTADMIN}\"+g" ${BACKUPDIR}/01_users
#sed -i "s+password_pbkdf2 root+password_pbkdf2 ${BOOTADMIN}+g" ${BACKUPDIR}/01_users

OLDDATA="password_pbkdf2 root"
NEWDATA="password_pbkdf2 ${BOOTADMIN}"
sed -i 's+'"${OLDDATA}"'+'"${NEWDATA}"'+' ${BACKUPDIR}/01_users


/bin/cp -fpd ${BACKUPDIR}/01_users /etc/grub.d/01_users

#this doesn't appear to work
#grubby --update-kernel=ALL --env=/boot/grub2/grubenv

# determine if your installation uses UEFI or BIOS
[ -d /sys/firmware/efi ] && echo UEFI || echo BIOS

# If UEFI use:
grub2-mkconfig -o /boot/efi/EFI/rocky/grub.cfg

# If BIOS use:
grub2-mkconfig -o /boot/grub2/grub.cfg

# Set Boot Loader Password in grub2 for BOOTADMIN account defined above
# CIS 1.4.1

grub2-setpassword

EOFPASSWD

#########################
## DEPLOY CONFIGURATION
#########################

# Enable Auditing for Processes Which Start Prior to the Audit Daemon
# CIS 4.1.1.3
# edit /etc/default/grub append line GRUB_CMDLINE_LINUX with "audit=1"
grubby --update-kernel=ALL --args="audit=1"

# Extend Audit Backlog Limit for the Audit Daemon
# CIS 4.1.1.4
# edit /etc/default/grub append line GRUB_CMDLINE_LINUX with  "audit_backlog_limit=8192"
grubby --update-kernel=ALL --args="audit_backlog_limit=8192"

# Disable vsyscalls
# CUI
# edit /etc/default/grub append line GRUB_CMDLINE_LINUX with "vsyscall=none"
grubby --update-kernel=ALL --args="vsyscall=none"

## Memory Poisoning
# Enable page allocator poisoning
# CUI
# edit /etc/default/grub append line GRUB_CMDLINE_LINUX with "page_poison=1"
#grubby --update-kernel=ALL --args="page_poison=1"

# Enable SLUB/SLAB allocator poisoning
# CUI
# edit /etc/default/grub append line GRUB_CMDLINE_LINUX with "slub_debug=P"
#grubby --update-kernel=ALL --args="slub_debug=P"

# Enable Randomization of the page allocator
# CUI
# edit /etc/default/grub append line GRUB_CMDLINE_LINUX with "page_alloc.shuffle=1"
grubby --update-kernel=ALL --args="page_alloc.shuffle=1"

# Configure kernel to zero out memory before allocation
# CUI
# edit /etc/default/grub append line GRUB_CMDLINE_LINUX with "init_on_alloc=1"
grubby --update-kernel=ALL --args="init_on_alloc=1"

# Enable Kernel Page-Table Isolation (KPTI)
# STIG
# edit /etc/default/grub append line GRUB_CMDLINE_LINUX with "pti=on"
grubby --update-kernel=ALL --args="pti=on"


#timestamp
echo "** el9-grub COMPLETE" $(date +%F-%H%M-%S)

%end
