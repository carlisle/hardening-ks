##############################################################################
## alma9-oscap
## Copyright (C) 2024 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
## /root/oscap_results/*
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
## AlmaLinux    https://almalinux.org/
## Documenation https://wiki.almalinux.org/
## AlmaLinux 9 OpenSCAP Guide
## https://wiki.almalinux.org/documentation/openscap-guide-for-9.html
##
## This guide presents a catalog of security-relevant configuration settings
## for AlmaLinux 9. It is a rendering of content structured in the eXtensible
## Configuration Checklist Description Format (XCCDF) in order to support
## security automation. The SCAP content is is available in the 
## scap-security-guide package which is developed at 
## https://www.open-scap.org/security-policies/scap-security-guide.
##
## Providing system administrators with such guidance informs them how to
## securely configure systems under their control in a variety of network
## roles. Policy makers and baseline creators can use this catalog of
## settings, with its associated references to higher-level security control
## catalogs, in order to assist them in security baseline creation. This guide
## is a catalog, not a checklist, and satisfaction of every item is not likely
## to be possible or sensible in many operational scenarios. However, the XCCDF
## format enables granular selection and adjustment of settings, and their
## association with OVAL and OCIL content provides an automated checking
## capability. Transformations of this document, and its associated automated
## checking content, are capable of providing baselines that meet a diverse set
## of policy objectives. Some example XCCDF Profiles, which are selections
## of items that form checklists and can be used as baselines, are available
## with this guide. They can be processed, in an automated fashion, with tools
## that support the Security Content Automation Protocol (SCAP). The DISA STIG,
## which provides required settings for US Department of Defense systems, is
## one example of a baseline created from this guidance.
##
## These scripts are inspired by various Benchmarks by The Center for Internet
## Security. However, these scripts have not been reviewed or approved by CIS
## and they do not guarantee that their use will result in compliance with the
## CIS baseline. 
##
## This benchmark is a direct port of a SCAP Security Guide benchmark developed
## for Red Hat Enterprise Linux. It has been modified through an automated
## process to remove specific dependencies on Red Hat Enterprise Linux and to
## function with Almalinux. The result is a generally useful SCAP Security
## Guide benchmark with the following caveats:
##
## Almalinux is not an exact copy of Red Hat Enterprise Linux. There may be
## configuration differences that produce false positives and/or false
## negatives. If this occurs please file a bug report.
##
## Almalinux is a Linux distribution produced by the AlmaLinux OS Foundation.
## It is a free and open source operating system based on Red Hat Enterprise
## Linux with the goal "to be 100% bug-for-bug compatible" with its upstream
## commercial distribution. Almalinux has its own build system, compiler
## options, patchsets, and is a community supported, non-commercial
## operating system. Almainux does not inherit certifications or evaluations
## from Red Hat Enterprise Linux. As such, some configuration rules (such as
## those requiring FIPS 140-2 encryption) will continue to fail on Almainux.
##
## Members of the Almalinux community are invited to participate in OpenSCAP
## and SCAP Security Guide development. Bug reports and patches can be sent to
## GitHub: https://github.com/ComplianceAsCode/content. The mailing list is at
## https://fedorahosted.org/mailman/listinfo/scap-security-guide.
##
## Do not attempt to implement any of the settings in this guide without first
## testing them in a non-operational environment. The creators of this guidance
## assume no responsibility whatsoever for its use by other parties, and makes
## no guarantees, expressed or implied, about its quality, reliability, or any
## other characteristic.
##
##############################################################################
## Notes
## OpenSCAP User Manual
## https://static.open-scap.org/openscap-1.2/oscap_user_manual.html
##
##############################################################################

%packages

scap-security-guide
tar
gzip
unzip
bzip2

%end


## Find profiles:
## oscap info /usr/share/xml/scap/content/ssg-almalinux9-ds.xml | grep profile | sed 's+.*profile_++'
## Profiles: anssi_bp28_enhanced anssi_bp28_high anssi_bp28_intermediary anssi_bp28_minimal
## cis cis_server_l1 cis_workstation_l1 cis_workstation_l2
## cui e8 hipaa ism_o ospp pci-dss
## stig stig_gui


%post --log=/root/alma9-oscap.log

#timestamp
echo "** alma9-oscap START" $(date +%F-%H%M-%S)

##################
## SET VARIBLES
##################

BACKUPDIR=/root/KShardening/oscap/
OSCAPDIR=/root/oscap_results/
AUDITDIR=/root/KShardening/audit-scripts/


#################
## BACKUP FILES
#################

if [ ! -d "${BACKUPDIR}" ]; then mkdir -p ${BACKUPDIR}; fi
if [ ! -d "${OSCAPDIR}" ];  then mkdir -p ${OSCAPDIR}; fi
if [ ! -d "${AUDITDIR}" ]; then mkdir -p ${AUDITDIR}; fi


####################
## WRITE NEW FILES
####################

# Here Files
# http://tldp.org/LDP/abs/html/here-docs.html
# Note: No parameter substitution when the "limit string" is quoted or escaped.

curl https://bitbucket.org/carlisle/hardening-ks/raw/master/almalinux9/oscap-evaluate_almalinux9_0.1.74.sh \
     -o ${OSCAPDIR}/oscap-evaluate_almalinux9_0.1.74.sh

##########################
## security audit script
##########################

cat > ${BACKUPDIR}/audit_security.sh << 'EOF'
# Security Tests

printf "========================================\n"
printf "= Performing Security Tests = $(date +%F) at $(date +%H%M) =\n"
printf "========================================\n"
printf "List all filesystems: \n\n"
df --local -P | awk {'if (NR!=1) print $6'}

printf "========================================\n"
printf "Show system executables that don't have root ownership: \n\n"
find /bin/ /usr/bin/ /usr/local/bin/ /sbin/ /usr/sbin/ /usr/local/sbin/ /usr/libexec \! -user root -exec ls -l {} \;

printf "========================================\n"
printf "Show files that differ from expected file hashes\n"
printf "These will report files modified due to hardening: \n\n"
rpm -Va | grep '^..5'

printf "========================================\n"
printf "Find SUID Executables in local filesystems:\n\n"
## CIS 6.1.13 Audit SUID executables
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type f -perm -4000 -print
# to search selected file systems use:
# find ${filesystem} -xdev -type f -perm -4000

printf "========================================\n"
printf "Verfiy integrity of the SUID binaries returned by above:\n\n"
## CCE-80133-2 Ensure All SUID Executables Are Authorized
SUIDFILES=$(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type f -perm -4000 -print)
for I in $SUIDFILES; do echo "Integrity of $I:  "; rpm -V $(rpm -qf $I ); echo; done

printf "========================================\n"
printf "Find SGID Executables in local filesystems:\n\n"
# CIS 6.1.14 Audit SGID executables
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type f -perm -2000 -print
# to search selected file systems use:
# find ${filesystem} -xdev -type f -perm -2000

printf "========================================\n"
printf "Verfiy integrity of SGID binaries returned by above:\n\n"
## CCE-80132-4 Ensure All SGID Executables Are Authorized 
SGIDFILES=$(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type f -perm -2000 -print)
for I in $SGIDFILES; do echo "Integrity of $I:  "; rpm -V $(rpm -qf $I ); echo; done

printf "========================================\n"
printf "Show all World-Writable Directories that don't have the Sticky Bits Set:\n\n"
## CIS 1.1.21  Ensure sticky bit is set on all world-writable directories
## CCE-80130-8 Verify that All World-Writable Directories Have Sticky Bits Set
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null

printf "========================================\n"
printf "Show all World-Writable Files:\n\n"
## CIS 9.1.10 Find World Writable Files
## CCE-80131-6 Ensure No World-Writable Files Exist
# Ensure No World-Writable Files Exist
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type f -perm -0002

printf "========================================\n"
printf "Show Un-owned Files and Directories in local file systems\n\n"
# Ensure All Files Are Owned by a User
# CIS 6.1.11 Ensure no unowned files or directories exist 
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -nouser -ls
# to search selected file systems use:
# find ${filesystem} -xdev -nouser

printf "========================================\n"
printf "Show Un-grouped Files and Directories in local file systems\n\n"
# Ensure All Files Are Owned by a Group
# CIS 6.1.12 Ensure no ungrouped files or directories exist
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -nogroup -ls
# to search selected file systems use:
# find ${filesystem} -xdev -nogroup

printf "========================================\n"
printf "Ensure All World-Writable Directories Are Owned by a System Account\n\n"
# Ensure All World-Writable Directories Are Owned by a System Account
# Assumes system accounts have uid < $FIRSTUSER
FIRSTUSER=1000
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type d -perm -0002 -uid +$FIRSTUSER -print

printf "========================================\n"
printf "Verify integrity of passwd, shadow, and group files\n\n"
# Verify integrity of passwd shadow and group files

pwck -r
grpck -r 

printf "========================================\n"
printf "Show all empty password fields\n\n"
# CIS 6.2.1 Ensure password fields are not empty
/bin/cat /etc/shadow | /bin/awk -F: '($2 == "" ) { print $1 " does not have a password "}' 

printf "========================================\n"
printf "Show umask in bashrc and profile\n\n"
# CIS 5.4.4 Ensure default user umask is 027 or more restrictive
grep "umask" /etc/bashrc /etc/profile /etc/profile.d/*.sh

printf "========================================\n"
printf "Show user directories with .rhosts, .netrc, or .forward files\n\n"
# CIS 6.2.14 Ensure no users have .rhosts files
# CIS 6.2.12 Ensure no users have .netrc files
# CIS 6.2.11 Ensure no users have .forward files
HOMEDIR=$(/bin/cat /etc/passwd | /bin/egrep -v '(root|halt|sync|shutdown)' \
 | /bin/awk -F: '($7 != "/sbin/nologin") { print $6 }')
for file in ${HOMEDIR}/.rhosts; do
    if [ ! -h "${FILE}" -a -f "${FILE}" ]; then
      echo ".rhosts file ${HOMEDIR}/.rhosts exists"
      echo ".netrc file ${HOMEDIR}/.netrc exists"
      echo ".forward file ${HOMEDIR}/.forward exists"
    fi 
done

printf "========================================\n"
printf "sysctl configuration:\n"
printf "The following should be set to 0:\n\n"

sysctl --all | grep net.ipv4.conf.default.send_redirects
sysctl --all | grep net.ipv4.conf.all.send_redirects
sysctl --all | grep "net.ipv4.ip_forward "
sysctl --all | grep net.ipv4.conf.all.accept_source_route
sysctl --all | grep net.ipv4.conf.all.accept_redirects
sysctl --all | grep net.ipv4.conf.all.secure_redirects
sysctl --all | grep fs.suid_dumpable 

printf "\nThe following should be set to 1:\n\n" 
sysctl --all | grep net.ipv4.conf.all.log_martians
sysctl --all | grep net.ipv4.conf.default.log_martians
sysctl --all | grep net.ipv4.icmp_echo_ignore_broadcasts
sysctl --all | grep net.ipv4.icmp_ignore_bogus_error_responses

printf "\nThe following should be set to 0:\n\n"
sysctl --all | grep net.ipv4.conf.default.accept_source_route
sysctl --all | grep net.ipv4.conf.all.accept_redirects
sysctl --all | grep net.ipv4.conf.default.secure_redirects
#sysctl --all | grep net.ipv4.icmp_echo_ignore_broadcasts
#sysctl --all | grep net.ipv4.icmp_ignore_bogus_error_responses
sysctl --all | grep net.ipv4.tcp_syncookies
sysctl --all | grep net.ipv4.conf.all.rp_filter
sysctl --all | grep net.ipv4.conf.default.rp_filter
sysctl --all | grep net.ipv6.conf.all.disable_ipv6
sysctl --all | grep "net.ipv6.conf.all.accept_ra "
sysctl --all | grep "net.ipv6.conf.default.accept_ra "
sysctl --all | grep net.ipv6.conf.all.accept_redirects
sysctl --all | grep net.ipv6.conf.default.accept_redirects

printf "========================================\n"
printf "other\n\n"
 
#are these running: rxinetd telnet-server rsh-server ypserv tftp-server
#find -type f -name .rhosts -exec rm -f '{}' \;
#rm /etc/hosts.equiv

EOF


#####################
## DEPLOY NEW FILES
#####################

chown root:root ${OSCAPDIR}/oscap-evaluate_*.sh
chmod 700       ${OSCAPDIR}/oscap-evaluate_*.sh


#timestamp
echo "** alma9-oscap COMPLETE" $(date +%F-%H%M-%S)

%end
