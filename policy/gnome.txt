

## Enable GNOME3 Login Warning Banner
## Set the GNOME3 Login Warning Banner Text

#edit file /etc/dconf/db/distro.d/00-security-settings

[org/gnome/login-screen]
banner-message-enable=true
banner-message-text='APPROVED_BANNER'

#add lock to /etc/dconf/db/distro.d/locks/00-security-settings-lock

/org/gnome/login-screen/banner-message-enable
/org/gnome/login-screen/banner-message-text

# After changes run: dconf update



